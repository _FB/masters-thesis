/**
 */
package refinement.model.refinements.provider;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.RefinementsPackage;
import refinement.model.refinements.States;

/**
 * This is the item provider adapter for a {@link refinement.model.refinements.AtomicRefinement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AtomicRefinementItemProvider extends AbstractRefinementItemProvider {
    /**
     * This constructs an instance from a factory and a notifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AtomicRefinementItemProvider(AdapterFactory adapterFactory) {
        super(adapterFactory);
    }

    /**
     * This returns the property descriptors for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
        if (itemPropertyDescriptors == null) {
            super.getPropertyDescriptors(object);

            addValuePropertyDescriptor(object);
            addReferenceIDPropertyDescriptor(object);
            addDifferencePropertyDescriptor(object);
            addKindPropertyDescriptor(object);
            addValueStatesPropertyDescriptor(object);
            addStatePropertyDescriptor(object);
            addComposedRefinementsPropertyDescriptor(object);
            addReferenceTypePropertyDescriptor(object);
            addKindDetailPropertyDescriptor(object);
        }
        return itemPropertyDescriptors;
    }

    /**
     * This adds a property descriptor for the Value feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addValuePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_value_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_value_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__VALUE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Reference ID feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addReferenceIDPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_referenceID_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_referenceID_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__REFERENCE_ID,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Difference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addDifferencePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_difference_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_difference_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__DIFFERENCE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Kind feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addKindPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_kind_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_kind_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__KIND,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Value States feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addValueStatesPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_valueStates_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_valueStates_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__VALUE_STATES,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the State feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addStatePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_state_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_state_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__STATE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Composed Refinements feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addComposedRefinementsPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_composedRefinements_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_composedRefinements_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS,
                 true,
                 false,
                 true,
                 null,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Reference Type feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addReferenceTypePropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_referenceType_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_referenceType_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__REFERENCE_TYPE,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This adds a property descriptor for the Kind Detail feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void addKindDetailPropertyDescriptor(Object object) {
        itemPropertyDescriptors.add
            (createItemPropertyDescriptor
                (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
                 getResourceLocator(),
                 getString("_UI_AtomicRefinement_kindDetail_feature"),
                 getString("_UI_PropertyDescriptor_description", "_UI_AtomicRefinement_kindDetail_feature", "_UI_AtomicRefinement_type"),
                 RefinementsPackage.Literals.ATOMIC_REFINEMENT__KIND_DETAIL,
                 true,
                 false,
                 false,
                 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
                 null,
                 null));
    }

    /**
     * This returns AtomicRefinement.gif.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object getImage(Object object) {
        return overlayImage(object, getResourceLocator().getImage("full/obj16/AtomicRefinement"));
    }

    /**
     * This returns the label text for the adapted class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    @Override
    public String getText(Object object) {
        String label = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        States state = ((AtomicRefinement) object).getState();
        label = ((AtomicRefinement) object).getKind() + " " + ((AtomicRefinement) object).getKindDetail() + ": '" + ((AtomicRefinement) object).getValue()
                + "' [ReferenceID:  " + ((AtomicRefinement) object).getReferenceID() + "] ";
        Date date = ((AtomicRefinement) object).getDate();
        if (date != null) {
            label = label + "[Created: " + dateFormat.format(date) + "]";
        }
        if (state != null) {
            if (state.equals(States.TODO)) {
                label = "[State: " + state + "] " + label;
            }
        }
        return label;
    }
    

    /**
     * This handles model notifications by calling {@link #updateChildren} to update any cached
     * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void notifyChanged(Notification notification) {
        updateChildren(notification);

        switch (notification.getFeatureID(AtomicRefinement.class)) {
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE:
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID:
            case RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE:
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND:
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES:
            case RefinementsPackage.ATOMIC_REFINEMENT__STATE:
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE:
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL:
                fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
                return;
        }
        super.notifyChanged(notification);
    }

    /**
     * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
     * that can be created under this object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);
    }

}
