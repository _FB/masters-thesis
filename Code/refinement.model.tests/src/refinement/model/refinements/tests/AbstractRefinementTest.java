/**
 */
package refinement.model.refinements.tests;

import junit.framework.TestCase;

import refinement.model.refinements.AbstractRefinement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AbstractRefinementTest extends TestCase {

    /**
     * The fixture for this Abstract Refinement test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AbstractRefinement fixture = null;

    /**
     * Constructs a new Abstract Refinement test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AbstractRefinementTest(String name) {
        super(name);
    }

    /**
     * Sets the fixture for this Abstract Refinement test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected void setFixture(AbstractRefinement fixture) {
        this.fixture = fixture;
    }

    /**
     * Returns the fixture for this Abstract Refinement test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AbstractRefinement getFixture() {
        return fixture;
    }

} //AbstractRefinementTest
