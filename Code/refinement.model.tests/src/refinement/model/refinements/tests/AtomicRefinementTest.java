/**
 */
package refinement.model.refinements.tests;

import junit.textui.TestRunner;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.RefinementsFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Atomic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#setRefinement(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Set Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#addValueState(java.lang.String) <em>Add Value State</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#addValueStates(org.eclipse.emf.common.util.EList) <em>Add Value States</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class AtomicRefinementTest extends AbstractRefinementTest {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(AtomicRefinementTest.class);
    }

    /**
     * Constructs a new Atomic Refinement test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AtomicRefinementTest(String name) {
        super(name);
    }

    /**
     * Returns the fixture for this Atomic Refinement test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected AtomicRefinement getFixture() {
        return (AtomicRefinement)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(RefinementsFactory.eINSTANCE.createAtomicRefinement());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

    /**
     * Tests the '{@link refinement.model.refinements.AtomicRefinement#setRefinement(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Set Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.AtomicRefinement#setRefinement(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     * @generated
     */
    public void testSetRefinement__String_String_String_String_String_String_String() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link refinement.model.refinements.AtomicRefinement#addValueState(java.lang.String) <em>Add Value State</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.AtomicRefinement#addValueState(java.lang.String)
     * @generated
     */
    public void testAddValueState__String() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link refinement.model.refinements.AtomicRefinement#addValueStates(org.eclipse.emf.common.util.EList) <em>Add Value States</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.AtomicRefinement#addValueStates(org.eclipse.emf.common.util.EList)
     * @generated
     */
    public void testAddValueStates__EList() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

} //AtomicRefinementTest
