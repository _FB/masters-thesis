/**
 */
package refinement.model.refinements.tests;

import junit.textui.TestRunner;

import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.RefinementsFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Composed Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ComposedRefinementTest extends AbstractRefinementTest {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static void main(String[] args) {
        TestRunner.run(ComposedRefinementTest.class);
    }

    /**
     * Constructs a new Composed Refinement test case with the given name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ComposedRefinementTest(String name) {
        super(name);
    }

    /**
     * Returns the fixture for this Composed Refinement test case.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected ComposedRefinement getFixture() {
        return (ComposedRefinement)fixture;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
    @Override
    protected void setUp() throws Exception {
        setFixture(RefinementsFactory.eINSTANCE.createComposedRefinement());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
    @Override
    protected void tearDown() throws Exception {
        setFixture(null);
    }

} //ComposedRefinementTest
