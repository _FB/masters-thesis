/**
 */
package refinement.model.refinements.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Refinements</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefinementsAllTests extends TestSuite {

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static void main(String[] args) {
        TestRunner.run(suite());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static Test suite() {
        TestSuite suite = new RefinementsAllTests("Refinements Tests");
        suite.addTest(RefinementsTests.suite());
        return suite;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public RefinementsAllTests(String name) {
        super(name);
    }

} //RefinementsAllTests
