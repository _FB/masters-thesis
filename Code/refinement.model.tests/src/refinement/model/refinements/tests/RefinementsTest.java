/**
 */
package refinement.model.refinements.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import refinement.model.refinements.Refinements;
import refinement.model.refinements.RefinementsFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Refinements</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link refinement.model.refinements.Refinements#addRefinement(refinement.model.refinements.AbstractRefinement) <em>Add Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#saveRefinements(java.lang.String) <em>Save Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#indexOfRefinement(refinement.model.refinements.AtomicRefinement) <em>Index Of Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#getRefinement(int) <em>Get Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#setRefinement(int, refinement.model.refinements.AtomicRefinement) <em>Set Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#findRefinementsByReference(java.lang.String) <em>Find Refinements By Reference</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#removeRefinement(int) <em>Remove Refinement</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#getReplaceIndex(org.eclipse.emf.common.util.EList) <em>Get Replace Index</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class RefinementsTest extends TestCase {

	/**
     * The fixture for this Refinements test case.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected Refinements fixture = null;

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static void main(String[] args) {
        TestRunner.run(RefinementsTest.class);
    }

	/**
     * Constructs a new Refinements test case with the given name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public RefinementsTest(String name) {
        super(name);
    }

	/**
     * Sets the fixture for this Refinements test case.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void setFixture(Refinements fixture) {
        this.fixture = fixture;
    }

	/**
     * Returns the fixture for this Refinements test case.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected Refinements getFixture() {
        return fixture;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see junit.framework.TestCase#setUp()
     * @generated
     */
	@Override
	protected void setUp() throws Exception {
        setFixture(RefinementsFactory.eINSTANCE.createRefinements());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see junit.framework.TestCase#tearDown()
     * @generated
     */
	@Override
	protected void tearDown() throws Exception {
        setFixture(null);
    }

	/**
     * Tests the '{@link refinement.model.refinements.Refinements#addRefinement(refinement.model.refinements.AbstractRefinement) <em>Add Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#addRefinement(refinement.model.refinements.AbstractRefinement)
     * @generated
     */
    public void testAddRefinement__AbstractRefinement() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link refinement.model.refinements.Refinements#saveRefinements(java.lang.String) <em>Save Refinements</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#saveRefinements(java.lang.String)
     * @generated
     */
	public void testSaveRefinements__String() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

	/**
     * Tests the '{@link refinement.model.refinements.Refinements#indexOfRefinement(refinement.model.refinements.AtomicRefinement) <em>Index Of Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#indexOfRefinement(refinement.model.refinements.AtomicRefinement)
     * @generated
     */
    public void testIndexOfRefinement__AtomicRefinement() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link refinement.model.refinements.Refinements#getRefinement(int) <em>Get Refinement</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#getRefinement(int)
     * @generated
     */
	public void testGetRefinement__int() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

	/**
     * Tests the '{@link refinement.model.refinements.Refinements#setRefinement(int, refinement.model.refinements.AtomicRefinement) <em>Set Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#setRefinement(int, refinement.model.refinements.AtomicRefinement)
     * @generated
     */
    public void testSetRefinement__int_AtomicRefinement() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

    /**
     * Tests the '{@link refinement.model.refinements.Refinements#findRefinementsByReference(java.lang.String) <em>Find Refinements By Reference</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#findRefinementsByReference(java.lang.String)
     * @generated
     */
	public void testFindRefinementsByReference__String() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

	/**
     * Tests the '{@link refinement.model.refinements.Refinements#removeRefinement(int) <em>Remove Refinement</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#removeRefinement(int)
     * @generated
     */
	public void testRemoveRefinement__int() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

	/**
     * Tests the '{@link refinement.model.refinements.Refinements#getReplaceIndex(org.eclipse.emf.common.util.EList) <em>Get Replace Index</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.Refinements#getReplaceIndex(org.eclipse.emf.common.util.EList)
     * @generated
     */
	public void testGetReplaceIndex__EList() {
        // TODO: implement this operation test method
        // Ensure that you remove @generated or mark it @generated NOT
        fail();
    }

} //RefinementsTest
