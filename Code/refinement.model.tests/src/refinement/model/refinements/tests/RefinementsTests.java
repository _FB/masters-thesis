/**
 */
package refinement.model.refinements.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>refinements</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefinementsTests extends TestSuite {

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static void main(String[] args) {
        TestRunner.run(suite());
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public static Test suite() {
        TestSuite suite = new RefinementsTests("refinements Tests");
        suite.addTestSuite(AtomicRefinementTest.class);
        suite.addTestSuite(RefinementsTest.class);
        return suite;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public RefinementsTests(String name) {
        super(name);
    }

} //RefinementsTests
