/**
 */
package refinement.model.refinements;

import java.util.Date;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.AbstractRefinement#getDate <em>Date</em>}</li>
 * </ul>
 *
 * @see refinement.model.refinements.RefinementsPackage#getAbstractRefinement()
 * @model abstract="true"
 * @generated
 */
public interface AbstractRefinement extends EObject {

    /**
     * Returns the value of the '<em><b>Date</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Date</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Date</em>' attribute.
     * @see #setDate(Date)
     * @see refinement.model.refinements.RefinementsPackage#getAbstractRefinement_Date()
     * @model
     * @generated
     */
    Date getDate();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AbstractRefinement#getDate <em>Date</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Date</em>' attribute.
     * @see #getDate()
     * @generated
     */
    void setDate(Date value);
} // AbstractRefinement
