/**
 */
package refinement.model.refinements;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atomic Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getValue <em>Value</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getReferenceID <em>Reference ID</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getDifference <em>Difference</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getKind <em>Kind</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getValueStates <em>Value States</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getState <em>State</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getComposedRefinements <em>Composed Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link refinement.model.refinements.AtomicRefinement#getKindDetail <em>Kind Detail</em>}</li>
 * </ul>
 *
 * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement()
 * @model extendedMetaData="kind='mixed'"
 * @generated
 */
public interface AtomicRefinement extends AbstractRefinement {  
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_Value()
     * @model
     * @generated
     */
    String getValue();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(String value);

    /**
     * Returns the value of the '<em><b>Reference ID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reference ID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reference ID</em>' attribute.
     * @see #setReferenceID(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_ReferenceID()
     * @model
     * @generated
     */
    String getReferenceID();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getReferenceID <em>Reference ID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reference ID</em>' attribute.
     * @see #getReferenceID()
     * @generated
     */
    void setReferenceID(String value);

    /**
     * Returns the value of the '<em><b>Difference</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Difference</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Difference</em>' attribute.
     * @see #setDifference(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_Difference()
     * @model
     * @generated
     */
    String getDifference();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getDifference <em>Difference</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Difference</em>' attribute.
     * @see #getDifference()
     * @generated
     */
    void setDifference(String value);

    /**
     * Returns the value of the '<em><b>Kind</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Kind</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Kind</em>' attribute.
     * @see #setKind(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_Kind()
     * @model
     * @generated
     */
    String getKind();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getKind <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Kind</em>' attribute.
     * @see #getKind()
     * @generated
     */
    void setKind(String value);

    /**
     * Returns the value of the '<em><b>Value States</b></em>' attribute list.
     * The list contents are of type {@link java.lang.String}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value States</em>' attribute list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Value States</em>' attribute list.
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_ValueStates()
     * @model
     * @generated
     */
    EList<String> getValueStates();

    /**
     * Returns the value of the '<em><b>State</b></em>' attribute.
     * The literals are from the enumeration {@link refinement.model.refinements.States}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>State</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>State</em>' attribute.
     * @see refinement.model.refinements.States
     * @see #setState(States)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_State()
     * @model
     * @generated
     */
    States getState();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getState <em>State</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>State</em>' attribute.
     * @see refinement.model.refinements.States
     * @see #getState()
     * @generated
     */
    void setState(States value);

    /**
     * Returns the value of the '<em><b>Composed Refinements</b></em>' reference list.
     * The list contents are of type {@link refinement.model.refinements.ComposedRefinement}.
     * It is bidirectional and its opposite is '{@link refinement.model.refinements.ComposedRefinement#getAtomicRefinements <em>Atomic Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Composed Refinements</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Composed Refinements</em>' reference list.
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_ComposedRefinements()
     * @see refinement.model.refinements.ComposedRefinement#getAtomicRefinements
     * @model opposite="atomicRefinements"
     * @generated
     */
    EList<ComposedRefinement> getComposedRefinements();

    /**
     * Returns the value of the '<em><b>Reference Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Reference Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Reference Type</em>' attribute.
     * @see #setReferenceType(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_ReferenceType()
     * @model
     * @generated
     */
    String getReferenceType();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getReferenceType <em>Reference Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Reference Type</em>' attribute.
     * @see #getReferenceType()
     * @generated
     */
    void setReferenceType(String value);

    /**
     * Returns the value of the '<em><b>Kind Detail</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Kind Detail</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Kind Detail</em>' attribute.
     * @see #setKindDetail(String)
     * @see refinement.model.refinements.RefinementsPackage#getAtomicRefinement_KindDetail()
     * @model
     * @generated
     */
    String getKindDetail();

    /**
     * Sets the value of the '{@link refinement.model.refinements.AtomicRefinement#getKindDetail <em>Kind Detail</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Kind Detail</em>' attribute.
     * @see #getKindDetail()
     * @generated
     */
    void setKindDetail(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Set value, reference, difference and kind to this refinement.
     * <!-- end-model-doc -->
     * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.value = value;\r\nthis.reference = reference;\r\nthis.difference = difference;\r\nthis.kind = kind;\r\nthis.addValueState(valueState);\r\nthis.addValueState(value);\r\nthis.date = new Date();\r\n'"
     * @generated
     */
    void setRefinement(String value, String reference, String difference, String kind, String valueState, String referenceType, String kindDetail);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='if (this.valueStates == null) {\r\n\tEList<String> valueStates = new BasicEList<String>();\r\n\tif(value != null){\r\n\t\tvalueStates.add(value);\r\n\t\tthis.valueStates = valueStates;\r\n\t}\r\n} else {\r\n\tif (!value.trim().isEmpty() && !this.valueStates.contains(value)) {\r\n\t\tthis.valueStates.add(value);\r\n\t}\r\n}'"
     * @generated
     */
    void addValueState(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @model statesMany="false"
     * @generated NOT
     */
    void addValueStates(EList<String> states);

} // AtomicRefinement
