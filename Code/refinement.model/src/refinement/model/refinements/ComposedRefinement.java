/**
 */
package refinement.model.refinements;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composed Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.ComposedRefinement#getName <em>Name</em>}</li>
 *   <li>{@link refinement.model.refinements.ComposedRefinement#getDescription <em>Description</em>}</li>
 *   <li>{@link refinement.model.refinements.ComposedRefinement#getAtomicRefinements <em>Atomic Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.ComposedRefinement#getComposedRefinements <em>Composed Refinements</em>}</li>
 * </ul>
 *
 * @see refinement.model.refinements.RefinementsPackage#getComposedRefinement()
 * @model
 * @generated
 */
public interface ComposedRefinement extends AbstractRefinement {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see refinement.model.refinements.RefinementsPackage#getComposedRefinement_Name()
     * @model
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link refinement.model.refinements.ComposedRefinement#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see refinement.model.refinements.RefinementsPackage#getComposedRefinement_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link refinement.model.refinements.ComposedRefinement#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Atomic Refinements</b></em>' reference list.
     * The list contents are of type {@link refinement.model.refinements.AtomicRefinement}.
     * It is bidirectional and its opposite is '{@link refinement.model.refinements.AtomicRefinement#getComposedRefinements <em>Composed Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Atomic Refinements</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Atomic Refinements</em>' reference list.
     * @see refinement.model.refinements.RefinementsPackage#getComposedRefinement_AtomicRefinements()
     * @see refinement.model.refinements.AtomicRefinement#getComposedRefinements
     * @model opposite="composedRefinements"
     * @generated
     */
    EList<AtomicRefinement> getAtomicRefinements();

    /**
     * Returns the value of the '<em><b>Composed Refinements</b></em>' reference list.
     * The list contents are of type {@link refinement.model.refinements.ComposedRefinement}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Composed Refinements</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Composed Refinements</em>' reference list.
     * @see refinement.model.refinements.RefinementsPackage#getComposedRefinement_ComposedRefinements()
     * @model
     * @generated
     */
    EList<ComposedRefinement> getComposedRefinements();

} // ComposedRefinement
