/**
 */
package refinement.model.refinements;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Refinements</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.Refinements#getId <em>Id</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#getAtomicRefinements <em>Atomic Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.Refinements#getComposedRefinements <em>Composed Refinements</em>}</li>
 * </ul>
 *
 * @see refinement.model.refinements.RefinementsPackage#getRefinements()
 * @model
 * @generated
 */
public interface Refinements extends EObject {
	/**
     * Returns the value of the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
     * @return the value of the '<em>Id</em>' attribute.
     * @see #setId(String)
     * @see refinement.model.refinements.RefinementsPackage#getRefinements_Id()
     * @model id="true"
     * @generated
     */
	String getId();

	/**
     * Sets the value of the '{@link refinement.model.refinements.Refinements#getId <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @param value the new value of the '<em>Id</em>' attribute.
     * @see #getId()
     * @generated
     */
	void setId(String value);

	/**
     * Returns the value of the '<em><b>Atomic Refinements</b></em>' containment reference list.
     * The list contents are of type {@link refinement.model.refinements.AtomicRefinement}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Atomic Refinements</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Atomic Refinements</em>' containment reference list.
     * @see refinement.model.refinements.RefinementsPackage#getRefinements_AtomicRefinements()
     * @model containment="true"
     * @generated
     */
    EList<AtomicRefinement> getAtomicRefinements();

    /**
     * Returns the value of the '<em><b>Composed Refinements</b></em>' containment reference list.
     * The list contents are of type {@link refinement.model.refinements.ComposedRefinement}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Composed Refinements</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Composed Refinements</em>' containment reference list.
     * @see refinement.model.refinements.RefinementsPackage#getRefinements_ComposedRefinements()
     * @model containment="true"
     * @generated
     */
    EList<ComposedRefinement> getComposedRefinements();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * If refinement kind of given refinement is of type "DELETE", check if an other refinement
     * exists for referenceID of given refinement. Replace this found refinement with given refinement if such a refinement exists.
     * If refinement kind of given refinement is not of type "DELETE" add given refinement to refinements if given refinement is not already included in refinements list.
     * <!-- end-model-doc -->
     * @model
     * @generated
     */
    void addRefinement(AbstractRefinement refinement);

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Save this refinement to savePath.
     * <!-- end-model-doc -->
     * @model
     * @generated
     */
	void saveRefinements(String savePath);

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Find and return index of given refinement in refinements. If refinements does not contain given refinement -1 is returned.
     * <!-- end-model-doc -->
     * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='EList<AtomarRefinement> existingRefinements = this.getRefinements();\r\n\t\tfor (int i = 0; i < existingRefinements.size(); i++) {\r\n\t\t\tif (existingRefinements.get(i).getReference().equals(refinement.getReference())\r\n\t\t\t\t\t&& existingRefinements.get(i).getKind().equals(refinement.getKind())) {\r\n\t\t\t\treturn i;\r\n\t\t\t}\r\n\t\t}\r\n\t\treturn -1;'"
     * @generated
     */
    int indexOfRefinement(AtomicRefinement refinement);

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Return refinement of given index.
     * <!-- end-model-doc -->
     * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return this.getRefinements().get(index);'"
     * @generated
     */
	AtomicRefinement getRefinement(int index);

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Set given refinement to given index in refinements list.
     * <!-- end-model-doc -->
     * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.getRefinements().set(index, refinement);'"
     * @generated
     */
    void setRefinement(int index, AtomicRefinement refinement);

    /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Return all indexes of refinements with given referenceID.
	 * <!-- end-model-doc -->
	 * @model many="false"
	 * @generated NOT
	 */
	EList<Integer> findRefinementsByReference(String referenceID);

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * <!-- begin-model-doc -->
     * Remove refinement from refinements by given index.
     * <!-- end-model-doc -->
     * @model
     * @generated
     */
	void removeRefinement(int refinementIndex);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Int" indexListMany="false"
	 * @generated NOT
	 */
	int getReplaceIndex(EList<Integer> indexList);

} // Refinements
