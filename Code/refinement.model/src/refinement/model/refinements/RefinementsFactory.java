/**
 */
package refinement.model.refinements;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see refinement.model.refinements.RefinementsPackage
 * @generated
 */
public interface RefinementsFactory extends EFactory {
	/**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	RefinementsFactory eINSTANCE = refinement.model.refinements.impl.RefinementsFactoryImpl.init();

	/**
     * Returns a new object of class '<em>Atomic Refinement</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Atomic Refinement</em>'.
     * @generated
     */
    AtomicRefinement createAtomicRefinement();

    /**
     * Returns a new object of class '<em>Refinements</em>'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return a new object of class '<em>Refinements</em>'.
     * @generated
     */
	Refinements createRefinements();

	/**
     * Returns a new object of class '<em>Composed Refinement</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Composed Refinement</em>'.
     * @generated
     */
    ComposedRefinement createComposedRefinement();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
	RefinementsPackage getRefinementsPackage();

} //RefinementsFactory
