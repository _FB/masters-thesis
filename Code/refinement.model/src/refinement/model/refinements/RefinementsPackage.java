/**
 */
package refinement.model.refinements;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see refinement.model.refinements.RefinementsFactory
 * @model kind="package"
 * @generated
 */
public interface RefinementsPackage extends EPackage {
	/**
     * The package name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNAME = "refinements";

	/**
     * The package namespace URI.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_URI = "http://www.example.org/refinements";

	/**
     * The package namespace name.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	String eNS_PREFIX = "refinements";

	/**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	RefinementsPackage eINSTANCE = refinement.model.refinements.impl.RefinementsPackageImpl.init();

	/**
     * The meta object id for the '{@link refinement.model.refinements.impl.AbstractRefinementImpl <em>Abstract Refinement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.impl.AbstractRefinementImpl
     * @see refinement.model.refinements.impl.RefinementsPackageImpl#getAbstractRefinement()
     * @generated
     */
    int ABSTRACT_REFINEMENT = 3;

    /**
     * The feature id for the '<em><b>Date</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ABSTRACT_REFINEMENT__DATE = 0;

    /**
     * The number of structural features of the '<em>Abstract Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ABSTRACT_REFINEMENT_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Abstract Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ABSTRACT_REFINEMENT_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link refinement.model.refinements.impl.AtomicRefinementImpl <em>Atomic Refinement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.impl.AtomicRefinementImpl
     * @see refinement.model.refinements.impl.RefinementsPackageImpl#getAtomicRefinement()
     * @generated
     */
    int ATOMIC_REFINEMENT = 0;

    /**
     * The feature id for the '<em><b>Date</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__DATE = ABSTRACT_REFINEMENT__DATE;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__VALUE = ABSTRACT_REFINEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Reference ID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__REFERENCE_ID = ABSTRACT_REFINEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Difference</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__DIFFERENCE = ABSTRACT_REFINEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Kind</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__KIND = ABSTRACT_REFINEMENT_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Value States</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__VALUE_STATES = ABSTRACT_REFINEMENT_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>State</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__STATE = ABSTRACT_REFINEMENT_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Composed Refinements</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS = ABSTRACT_REFINEMENT_FEATURE_COUNT + 6;

    /**
     * The feature id for the '<em><b>Reference Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__REFERENCE_TYPE = ABSTRACT_REFINEMENT_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Kind Detail</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT__KIND_DETAIL = ABSTRACT_REFINEMENT_FEATURE_COUNT + 8;

    /**
     * The number of structural features of the '<em>Atomic Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT_FEATURE_COUNT = ABSTRACT_REFINEMENT_FEATURE_COUNT + 9;

    /**
     * The operation id for the '<em>Set Refinement</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT___SET_REFINEMENT__STRING_STRING_STRING_STRING_STRING_STRING_STRING = ABSTRACT_REFINEMENT_OPERATION_COUNT + 0;

    /**
     * The operation id for the '<em>Add Value State</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT___ADD_VALUE_STATE__STRING = ABSTRACT_REFINEMENT_OPERATION_COUNT + 1;

    /**
     * The operation id for the '<em>Add Value States</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT___ADD_VALUE_STATES__ELIST = ABSTRACT_REFINEMENT_OPERATION_COUNT + 2;

    /**
     * The number of operations of the '<em>Atomic Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int ATOMIC_REFINEMENT_OPERATION_COUNT = ABSTRACT_REFINEMENT_OPERATION_COUNT + 3;

    /**
     * The meta object id for the '{@link refinement.model.refinements.impl.RefinementsImpl <em>Refinements</em>}' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.impl.RefinementsImpl
     * @see refinement.model.refinements.impl.RefinementsPackageImpl#getRefinements()
     * @generated
     */
	int REFINEMENTS = 1;

	/**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS__ID = 0;

	/**
     * The feature id for the '<em><b>Atomic Refinements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REFINEMENTS__ATOMIC_REFINEMENTS = 1;

    /**
     * The feature id for the '<em><b>Composed Refinements</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REFINEMENTS__COMPOSED_REFINEMENTS = 2;

    /**
     * The number of structural features of the '<em>Refinements</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS_FEATURE_COUNT = 3;

	/**
     * The operation id for the '<em>Add Refinement</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REFINEMENTS___ADD_REFINEMENT__ABSTRACTREFINEMENT = 0;

    /**
     * The operation id for the '<em>Save Refinements</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS___SAVE_REFINEMENTS__STRING = 1;

	/**
     * The operation id for the '<em>Index Of Refinement</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REFINEMENTS___INDEX_OF_REFINEMENT__ATOMICREFINEMENT = 2;

    /**
     * The operation id for the '<em>Get Refinement</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS___GET_REFINEMENT__INT = 3;

	/**
     * The operation id for the '<em>Set Refinement</em>' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int REFINEMENTS___SET_REFINEMENT__INT_ATOMICREFINEMENT = 4;

    /**
     * The operation id for the '<em>Find Refinements By Reference</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS___FIND_REFINEMENTS_BY_REFERENCE__STRING = 5;

	/**
     * The operation id for the '<em>Remove Refinement</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS___REMOVE_REFINEMENT__INT = 6;

	/**
     * The operation id for the '<em>Get Replace Index</em>' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS___GET_REPLACE_INDEX__ELIST = 7;

	/**
     * The number of operations of the '<em>Refinements</em>' class.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
	int REFINEMENTS_OPERATION_COUNT = 8;


	/**
     * The meta object id for the '{@link refinement.model.refinements.impl.ComposedRefinementImpl <em>Composed Refinement</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see refinement.model.refinements.impl.ComposedRefinementImpl
     * @see refinement.model.refinements.impl.RefinementsPackageImpl#getComposedRefinement()
     * @generated
     */
    int COMPOSED_REFINEMENT = 2;

    /**
     * The feature id for the '<em><b>Date</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT__DATE = ABSTRACT_REFINEMENT__DATE;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT__NAME = ABSTRACT_REFINEMENT_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT__DESCRIPTION = ABSTRACT_REFINEMENT_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Atomic Refinements</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS = ABSTRACT_REFINEMENT_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Composed Refinements</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS = ABSTRACT_REFINEMENT_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Composed Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT_FEATURE_COUNT = ABSTRACT_REFINEMENT_FEATURE_COUNT + 4;

    /**
     * The number of operations of the '<em>Composed Refinement</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    int COMPOSED_REFINEMENT_OPERATION_COUNT = ABSTRACT_REFINEMENT_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link refinement.model.refinements.States <em>States</em>}' enum.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see refinement.model.refinements.States
     * @see refinement.model.refinements.impl.RefinementsPackageImpl#getStates()
     * @generated
     */
	int STATES = 4;


	/**
     * Returns the meta object for class '{@link refinement.model.refinements.AtomicRefinement <em>Atomic Refinement</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Atomic Refinement</em>'.
     * @see refinement.model.refinements.AtomicRefinement
     * @generated
     */
    EClass getAtomicRefinement();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getValue()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_Value();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getReferenceID <em>Reference ID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Reference ID</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getReferenceID()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_ReferenceID();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getDifference <em>Difference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Difference</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getDifference()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_Difference();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getKind <em>Kind</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Kind</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getKind()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_Kind();

    /**
     * Returns the meta object for the attribute list '{@link refinement.model.refinements.AtomicRefinement#getValueStates <em>Value States</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute list '<em>Value States</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getValueStates()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_ValueStates();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getState <em>State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>State</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getState()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_State();

    /**
     * Returns the meta object for the reference list '{@link refinement.model.refinements.AtomicRefinement#getComposedRefinements <em>Composed Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Composed Refinements</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getComposedRefinements()
     * @see #getAtomicRefinement()
     * @generated
     */
    EReference getAtomicRefinement_ComposedRefinements();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getReferenceType <em>Reference Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Reference Type</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getReferenceType()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_ReferenceType();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AtomicRefinement#getKindDetail <em>Kind Detail</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Kind Detail</em>'.
     * @see refinement.model.refinements.AtomicRefinement#getKindDetail()
     * @see #getAtomicRefinement()
     * @generated
     */
    EAttribute getAtomicRefinement_KindDetail();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.AtomicRefinement#setRefinement(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Set Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Set Refinement</em>' operation.
     * @see refinement.model.refinements.AtomicRefinement#setRefinement(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     * @generated
     */
    EOperation getAtomicRefinement__SetRefinement__String_String_String_String_String_String_String();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.AtomicRefinement#addValueState(java.lang.String) <em>Add Value State</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Add Value State</em>' operation.
     * @see refinement.model.refinements.AtomicRefinement#addValueState(java.lang.String)
     * @generated
     */
    EOperation getAtomicRefinement__AddValueState__String();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.AtomicRefinement#addValueStates(org.eclipse.emf.common.util.EList) <em>Add Value States</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Add Value States</em>' operation.
     * @see refinement.model.refinements.AtomicRefinement#addValueStates(org.eclipse.emf.common.util.EList)
     * @generated
     */
    EOperation getAtomicRefinement__AddValueStates__EList();

    /**
     * Returns the meta object for class '{@link refinement.model.refinements.Refinements <em>Refinements</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for class '<em>Refinements</em>'.
     * @see refinement.model.refinements.Refinements
     * @generated
     */
	EClass getRefinements();

	/**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.Refinements#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see refinement.model.refinements.Refinements#getId()
     * @see #getRefinements()
     * @generated
     */
	EAttribute getRefinements_Id();

	/**
     * Returns the meta object for the containment reference list '{@link refinement.model.refinements.Refinements#getAtomicRefinements <em>Atomic Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Atomic Refinements</em>'.
     * @see refinement.model.refinements.Refinements#getAtomicRefinements()
     * @see #getRefinements()
     * @generated
     */
    EReference getRefinements_AtomicRefinements();

    /**
     * Returns the meta object for the containment reference list '{@link refinement.model.refinements.Refinements#getComposedRefinements <em>Composed Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the containment reference list '<em>Composed Refinements</em>'.
     * @see refinement.model.refinements.Refinements#getComposedRefinements()
     * @see #getRefinements()
     * @generated
     */
    EReference getRefinements_ComposedRefinements();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#addRefinement(refinement.model.refinements.AbstractRefinement) <em>Add Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Add Refinement</em>' operation.
     * @see refinement.model.refinements.Refinements#addRefinement(refinement.model.refinements.AbstractRefinement)
     * @generated
     */
    EOperation getRefinements__AddRefinement__AbstractRefinement();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#saveRefinements(java.lang.String) <em>Save Refinements</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Save Refinements</em>' operation.
     * @see refinement.model.refinements.Refinements#saveRefinements(java.lang.String)
     * @generated
     */
	EOperation getRefinements__SaveRefinements__String();

	/**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#indexOfRefinement(refinement.model.refinements.AtomicRefinement) <em>Index Of Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Index Of Refinement</em>' operation.
     * @see refinement.model.refinements.Refinements#indexOfRefinement(refinement.model.refinements.AtomicRefinement)
     * @generated
     */
    EOperation getRefinements__IndexOfRefinement__AtomicRefinement();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#getRefinement(int) <em>Get Refinement</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Refinement</em>' operation.
     * @see refinement.model.refinements.Refinements#getRefinement(int)
     * @generated
     */
	EOperation getRefinements__GetRefinement__int();

	/**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#setRefinement(int, refinement.model.refinements.AtomicRefinement) <em>Set Refinement</em>}' operation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the '<em>Set Refinement</em>' operation.
     * @see refinement.model.refinements.Refinements#setRefinement(int, refinement.model.refinements.AtomicRefinement)
     * @generated
     */
    EOperation getRefinements__SetRefinement__int_AtomicRefinement();

    /**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#findRefinementsByReference(java.lang.String) <em>Find Refinements By Reference</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Find Refinements By Reference</em>' operation.
     * @see refinement.model.refinements.Refinements#findRefinementsByReference(java.lang.String)
     * @generated
     */
	EOperation getRefinements__FindRefinementsByReference__String();

	/**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#removeRefinement(int) <em>Remove Refinement</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Remove Refinement</em>' operation.
     * @see refinement.model.refinements.Refinements#removeRefinement(int)
     * @generated
     */
	EOperation getRefinements__RemoveRefinement__int();

	/**
     * Returns the meta object for the '{@link refinement.model.refinements.Refinements#getReplaceIndex(org.eclipse.emf.common.util.EList) <em>Get Replace Index</em>}' operation.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for the '<em>Get Replace Index</em>' operation.
     * @see refinement.model.refinements.Refinements#getReplaceIndex(org.eclipse.emf.common.util.EList)
     * @generated
     */
	EOperation getRefinements__GetReplaceIndex__EList();

	/**
     * Returns the meta object for class '{@link refinement.model.refinements.ComposedRefinement <em>Composed Refinement</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Composed Refinement</em>'.
     * @see refinement.model.refinements.ComposedRefinement
     * @generated
     */
    EClass getComposedRefinement();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.ComposedRefinement#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see refinement.model.refinements.ComposedRefinement#getName()
     * @see #getComposedRefinement()
     * @generated
     */
    EAttribute getComposedRefinement_Name();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.ComposedRefinement#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see refinement.model.refinements.ComposedRefinement#getDescription()
     * @see #getComposedRefinement()
     * @generated
     */
    EAttribute getComposedRefinement_Description();

    /**
     * Returns the meta object for the reference list '{@link refinement.model.refinements.ComposedRefinement#getAtomicRefinements <em>Atomic Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Atomic Refinements</em>'.
     * @see refinement.model.refinements.ComposedRefinement#getAtomicRefinements()
     * @see #getComposedRefinement()
     * @generated
     */
    EReference getComposedRefinement_AtomicRefinements();

    /**
     * Returns the meta object for the reference list '{@link refinement.model.refinements.ComposedRefinement#getComposedRefinements <em>Composed Refinements</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the reference list '<em>Composed Refinements</em>'.
     * @see refinement.model.refinements.ComposedRefinement#getComposedRefinements()
     * @see #getComposedRefinement()
     * @generated
     */
    EReference getComposedRefinement_ComposedRefinements();

    /**
     * Returns the meta object for class '{@link refinement.model.refinements.AbstractRefinement <em>Abstract Refinement</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for class '<em>Abstract Refinement</em>'.
     * @see refinement.model.refinements.AbstractRefinement
     * @generated
     */
    EClass getAbstractRefinement();

    /**
     * Returns the meta object for the attribute '{@link refinement.model.refinements.AbstractRefinement#getDate <em>Date</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the meta object for the attribute '<em>Date</em>'.
     * @see refinement.model.refinements.AbstractRefinement#getDate()
     * @see #getAbstractRefinement()
     * @generated
     */
    EAttribute getAbstractRefinement_Date();

    /**
     * Returns the meta object for enum '{@link refinement.model.refinements.States <em>States</em>}'.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the meta object for enum '<em>States</em>'.
     * @see refinement.model.refinements.States
     * @generated
     */
	EEnum getStates();

	/**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @return the factory that creates the instances of the model.
     * @generated
     */
	RefinementsFactory getRefinementsFactory();

	/**
     * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
     * @generated
     */
	interface Literals {
		/**
         * The meta object literal for the '{@link refinement.model.refinements.impl.AtomicRefinementImpl <em>Atomic Refinement</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see refinement.model.refinements.impl.AtomicRefinementImpl
         * @see refinement.model.refinements.impl.RefinementsPackageImpl#getAtomicRefinement()
         * @generated
         */
        EClass ATOMIC_REFINEMENT = eINSTANCE.getAtomicRefinement();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__VALUE = eINSTANCE.getAtomicRefinement_Value();

        /**
         * The meta object literal for the '<em><b>Reference ID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__REFERENCE_ID = eINSTANCE.getAtomicRefinement_ReferenceID();

        /**
         * The meta object literal for the '<em><b>Difference</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__DIFFERENCE = eINSTANCE.getAtomicRefinement_Difference();

        /**
         * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__KIND = eINSTANCE.getAtomicRefinement_Kind();

        /**
         * The meta object literal for the '<em><b>Value States</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__VALUE_STATES = eINSTANCE.getAtomicRefinement_ValueStates();

        /**
         * The meta object literal for the '<em><b>State</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__STATE = eINSTANCE.getAtomicRefinement_State();

        /**
         * The meta object literal for the '<em><b>Composed Refinements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS = eINSTANCE.getAtomicRefinement_ComposedRefinements();

        /**
         * The meta object literal for the '<em><b>Reference Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__REFERENCE_TYPE = eINSTANCE.getAtomicRefinement_ReferenceType();

        /**
         * The meta object literal for the '<em><b>Kind Detail</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ATOMIC_REFINEMENT__KIND_DETAIL = eINSTANCE.getAtomicRefinement_KindDetail();

        /**
         * The meta object literal for the '<em><b>Set Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation ATOMIC_REFINEMENT___SET_REFINEMENT__STRING_STRING_STRING_STRING_STRING_STRING_STRING = eINSTANCE.getAtomicRefinement__SetRefinement__String_String_String_String_String_String_String();

        /**
         * The meta object literal for the '<em><b>Add Value State</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation ATOMIC_REFINEMENT___ADD_VALUE_STATE__STRING = eINSTANCE.getAtomicRefinement__AddValueState__String();

        /**
         * The meta object literal for the '<em><b>Add Value States</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation ATOMIC_REFINEMENT___ADD_VALUE_STATES__ELIST = eINSTANCE.getAtomicRefinement__AddValueStates__EList();

        /**
         * The meta object literal for the '{@link refinement.model.refinements.impl.RefinementsImpl <em>Refinements</em>}' class.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see refinement.model.refinements.impl.RefinementsImpl
         * @see refinement.model.refinements.impl.RefinementsPackageImpl#getRefinements()
         * @generated
         */
		EClass REFINEMENTS = eINSTANCE.getRefinements();

		/**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EAttribute REFINEMENTS__ID = eINSTANCE.getRefinements_Id();

		/**
         * The meta object literal for the '<em><b>Atomic Refinements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference REFINEMENTS__ATOMIC_REFINEMENTS = eINSTANCE.getRefinements_AtomicRefinements();

        /**
         * The meta object literal for the '<em><b>Composed Refinements</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference REFINEMENTS__COMPOSED_REFINEMENTS = eINSTANCE.getRefinements_ComposedRefinements();

        /**
         * The meta object literal for the '<em><b>Add Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation REFINEMENTS___ADD_REFINEMENT__ABSTRACTREFINEMENT = eINSTANCE.getRefinements__AddRefinement__AbstractRefinement();

        /**
         * The meta object literal for the '<em><b>Save Refinements</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation REFINEMENTS___SAVE_REFINEMENTS__STRING = eINSTANCE.getRefinements__SaveRefinements__String();

		/**
         * The meta object literal for the '<em><b>Index Of Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation REFINEMENTS___INDEX_OF_REFINEMENT__ATOMICREFINEMENT = eINSTANCE.getRefinements__IndexOfRefinement__AtomicRefinement();

        /**
         * The meta object literal for the '<em><b>Get Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation REFINEMENTS___GET_REFINEMENT__INT = eINSTANCE.getRefinements__GetRefinement__int();

		/**
         * The meta object literal for the '<em><b>Set Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EOperation REFINEMENTS___SET_REFINEMENT__INT_ATOMICREFINEMENT = eINSTANCE.getRefinements__SetRefinement__int_AtomicRefinement();

        /**
         * The meta object literal for the '<em><b>Find Refinements By Reference</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation REFINEMENTS___FIND_REFINEMENTS_BY_REFERENCE__STRING = eINSTANCE.getRefinements__FindRefinementsByReference__String();

		/**
         * The meta object literal for the '<em><b>Remove Refinement</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation REFINEMENTS___REMOVE_REFINEMENT__INT = eINSTANCE.getRefinements__RemoveRefinement__int();

		/**
         * The meta object literal for the '<em><b>Get Replace Index</b></em>' operation.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @generated
         */
		EOperation REFINEMENTS___GET_REPLACE_INDEX__ELIST = eINSTANCE.getRefinements__GetReplaceIndex__EList();

		/**
         * The meta object literal for the '{@link refinement.model.refinements.impl.ComposedRefinementImpl <em>Composed Refinement</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see refinement.model.refinements.impl.ComposedRefinementImpl
         * @see refinement.model.refinements.impl.RefinementsPackageImpl#getComposedRefinement()
         * @generated
         */
        EClass COMPOSED_REFINEMENT = eINSTANCE.getComposedRefinement();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COMPOSED_REFINEMENT__NAME = eINSTANCE.getComposedRefinement_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute COMPOSED_REFINEMENT__DESCRIPTION = eINSTANCE.getComposedRefinement_Description();

        /**
         * The meta object literal for the '<em><b>Atomic Refinements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS = eINSTANCE.getComposedRefinement_AtomicRefinements();

        /**
         * The meta object literal for the '<em><b>Composed Refinements</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EReference COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS = eINSTANCE.getComposedRefinement_ComposedRefinements();

        /**
         * The meta object literal for the '{@link refinement.model.refinements.impl.AbstractRefinementImpl <em>Abstract Refinement</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @see refinement.model.refinements.impl.AbstractRefinementImpl
         * @see refinement.model.refinements.impl.RefinementsPackageImpl#getAbstractRefinement()
         * @generated
         */
        EClass ABSTRACT_REFINEMENT = eINSTANCE.getAbstractRefinement();

        /**
         * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * @generated
         */
        EAttribute ABSTRACT_REFINEMENT__DATE = eINSTANCE.getAbstractRefinement_Date();

        /**
         * The meta object literal for the '{@link refinement.model.refinements.States <em>States</em>}' enum.
         * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
         * @see refinement.model.refinements.States
         * @see refinement.model.refinements.impl.RefinementsPackageImpl#getStates()
         * @generated
         */
		EEnum STATES = eINSTANCE.getStates();

	}

} //RefinementsPackage
