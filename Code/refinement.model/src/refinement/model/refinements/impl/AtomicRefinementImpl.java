/**
 */
package refinement.model.refinements.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.RefinementsPackage;
import refinement.model.refinements.States;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atomic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getValue <em>Value</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getReferenceID <em>Reference ID</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getDifference <em>Difference</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getValueStates <em>Value States</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getState <em>State</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getComposedRefinements <em>Composed Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.AtomicRefinementImpl#getKindDetail <em>Kind Detail</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AtomicRefinementImpl extends AbstractRefinementImpl implements AtomicRefinement {
    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final String VALUE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected String value = VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getReferenceID() <em>Reference ID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferenceID()
     * @generated
     * @ordered
     */
    protected static final String REFERENCE_ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getReferenceID() <em>Reference ID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferenceID()
     * @generated
     * @ordered
     */
    protected String referenceID = REFERENCE_ID_EDEFAULT;

    /**
     * The default value of the '{@link #getDifference() <em>Difference</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDifference()
     * @generated
     * @ordered
     */
    protected static final String DIFFERENCE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDifference() <em>Difference</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDifference()
     * @generated
     * @ordered
     */
    protected String difference = DIFFERENCE_EDEFAULT;

    /**
     * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getKind()
     * @generated
     * @ordered
     */
    protected static final String KIND_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getKind()
     * @generated
     * @ordered
     */
    protected String kind = KIND_EDEFAULT;

    /**
     * The cached value of the '{@link #getValueStates() <em>Value States</em>}' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getValueStates()
     * @generated
     * @ordered
     */
    protected EList<String> valueStates;

    /**
     * The default value of the '{@link #getState() <em>State</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getState()
     * @generated
     * @ordered
     */
    protected static final States STATE_EDEFAULT = States.RESOLVED;

    /**
     * The cached value of the '{@link #getState() <em>State</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getState()
     * @generated
     * @ordered
     */
    protected States state = STATE_EDEFAULT;

    /**
     * The cached value of the '{@link #getComposedRefinements() <em>Composed Refinements</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComposedRefinements()
     * @generated
     * @ordered
     */
    protected EList<ComposedRefinement> composedRefinements;

    /**
     * The default value of the '{@link #getReferenceType() <em>Reference Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferenceType()
     * @generated
     * @ordered
     */
    protected static final String REFERENCE_TYPE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getReferenceType() <em>Reference Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getReferenceType()
     * @generated
     * @ordered
     */
    protected String referenceType = REFERENCE_TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getKindDetail() <em>Kind Detail</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getKindDetail()
     * @generated
     * @ordered
     */
    protected static final String KIND_DETAIL_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getKindDetail() <em>Kind Detail</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getKindDetail()
     * @generated
     * @ordered
     */
    protected String kindDetail = KIND_DETAIL_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected AtomicRefinementImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RefinementsPackage.Literals.ATOMIC_REFINEMENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setValue(String newValue) {
        String oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getReferenceID() {
        return referenceID;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setReferenceID(String newReferenceID) {
        String oldReferenceID = referenceID;
        referenceID = newReferenceID;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID, oldReferenceID, referenceID));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getDifference() {
        return difference;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDifference(String newDifference) {
        String oldDifference = difference;
        difference = newDifference;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE, oldDifference, difference));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getKind() {
        return kind;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setKind(String newKind) {
        String oldKind = kind;
        kind = newKind;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__KIND, oldKind, kind));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<String> getValueStates() {
        if (valueStates == null) {
            valueStates = new EDataTypeUniqueEList<String>(String.class, this, RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES);
        }
        return valueStates;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public States getState() {
        return state;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setState(States newState) {
        States oldState = state;
        state = newState == null ? STATE_EDEFAULT : newState;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__STATE, oldState, state));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<ComposedRefinement> getComposedRefinements() {
        if (composedRefinements == null) {
            composedRefinements = new EObjectWithInverseResolvingEList.ManyInverse<ComposedRefinement>(ComposedRefinement.class, this, RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS, RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS);
        }
        return composedRefinements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getReferenceType() {
        return referenceType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setReferenceType(String newReferenceType) {
        String oldReferenceType = referenceType;
        referenceType = newReferenceType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE, oldReferenceType, referenceType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getKindDetail() {
        return kindDetail;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setKindDetail(String newKindDetail) {
        String oldKindDetail = kindDetail;
        kindDetail = newKindDetail;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL, oldKindDetail, kindDetail));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void setRefinement(final String value, final String reference, final String difference, final String kind, final String valueState, final String kindDetail, final String referenceType) {
        this.value = value;
        this.referenceID = reference;
        this.difference = difference;
        this.kind = kind;
        this.addValueState(valueState);
        this.addValueState(value);
        this.date = new Date();
        this.kindDetail = kindDetail;
        this.referenceType = referenceType;
        
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void setRefinement(final String value, final String reference, final String difference, final String kind, final String valueState, final String referenceType) {
        this.value = value;
        this.referenceID = reference;
        this.difference = difference;
        this.kind = kind;
        this.addValueState(valueState);
        this.addValueState(value);
        this.date = new Date();
        this.referenceType = referenceType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void addValueState(final String value) {
        if (this.valueStates == null) {
            EList<String> valueStates = new BasicEList<String>();
            if(value != null){
                valueStates.add(value);
                this.valueStates = valueStates;
            }
        } else {
            if (!value.trim().isEmpty() && !this.valueStates.contains(value)) {
                this.valueStates.add(value);
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void addValueStates(EList<String> states) {
        this.getValueStates().addAll(states);
        // Create a new LinkedHashSet
        Set<String> set = new LinkedHashSet<>();
        // Add the elements to set
        set.addAll(this.getValueStates());
        // Clear the list
        this.getValueStates().clear();
        // add the elements of set
        // with no duplicates to the list
        this.getValueStates().addAll(set);
        }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getComposedRefinements()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                return ((InternalEList<?>)getComposedRefinements()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE:
                return getValue();
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID:
                return getReferenceID();
            case RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE:
                return getDifference();
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND:
                return getKind();
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES:
                return getValueStates();
            case RefinementsPackage.ATOMIC_REFINEMENT__STATE:
                return getState();
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                return getComposedRefinements();
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE:
                return getReferenceType();
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL:
                return getKindDetail();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE:
                setValue((String)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID:
                setReferenceID((String)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE:
                setDifference((String)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND:
                setKind((String)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES:
                getValueStates().clear();
                getValueStates().addAll((Collection<? extends String>)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__STATE:
                setState((States)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                getComposedRefinements().addAll((Collection<? extends ComposedRefinement>)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE:
                setReferenceType((String)newValue);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL:
                setKindDetail((String)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE:
                setValue(VALUE_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID:
                setReferenceID(REFERENCE_ID_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE:
                setDifference(DIFFERENCE_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND:
                setKind(KIND_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES:
                getValueStates().clear();
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__STATE:
                setState(STATE_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE:
                setReferenceType(REFERENCE_TYPE_EDEFAULT);
                return;
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL:
                setKindDetail(KIND_DETAIL_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE:
                return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_ID:
                return REFERENCE_ID_EDEFAULT == null ? referenceID != null : !REFERENCE_ID_EDEFAULT.equals(referenceID);
            case RefinementsPackage.ATOMIC_REFINEMENT__DIFFERENCE:
                return DIFFERENCE_EDEFAULT == null ? difference != null : !DIFFERENCE_EDEFAULT.equals(difference);
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND:
                return KIND_EDEFAULT == null ? kind != null : !KIND_EDEFAULT.equals(kind);
            case RefinementsPackage.ATOMIC_REFINEMENT__VALUE_STATES:
                return valueStates != null && !valueStates.isEmpty();
            case RefinementsPackage.ATOMIC_REFINEMENT__STATE:
                return state != STATE_EDEFAULT;
            case RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS:
                return composedRefinements != null && !composedRefinements.isEmpty();
            case RefinementsPackage.ATOMIC_REFINEMENT__REFERENCE_TYPE:
                return REFERENCE_TYPE_EDEFAULT == null ? referenceType != null : !REFERENCE_TYPE_EDEFAULT.equals(referenceType);
            case RefinementsPackage.ATOMIC_REFINEMENT__KIND_DETAIL:
                return KIND_DETAIL_EDEFAULT == null ? kindDetail != null : !KIND_DETAIL_EDEFAULT.equals(kindDetail);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID) {
            case RefinementsPackage.ATOMIC_REFINEMENT___SET_REFINEMENT__STRING_STRING_STRING_STRING_STRING_STRING_STRING:
                setRefinement((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
                return null;
            case RefinementsPackage.ATOMIC_REFINEMENT___ADD_VALUE_STATE__STRING:
                addValueState((String)arguments.get(0));
                return null;
            case RefinementsPackage.ATOMIC_REFINEMENT___ADD_VALUE_STATES__ELIST:
                addValueStates((EList)arguments.get(0));
                return null;
        }
        return super.eInvoke(operationID, arguments);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (value: ");
        result.append(value);
        result.append(", referenceID: ");
        result.append(referenceID);
        result.append(", difference: ");
        result.append(difference);
        result.append(", kind: ");
        result.append(kind);
        result.append(", valueStates: ");
        result.append(valueStates);
        result.append(", state: ");
        result.append(state);
        result.append(", referenceType: ");
        result.append(referenceType);
        result.append(", kindDetail: ");
        result.append(kindDetail);
        result.append(')');
        return result.toString();
    }

} //AtomicRefinementImpl
