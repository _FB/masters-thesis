/**
 */
package refinement.model.refinements.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.RefinementsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composed Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.impl.ComposedRefinementImpl#getName <em>Name</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.ComposedRefinementImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.ComposedRefinementImpl#getAtomicRefinements <em>Atomic Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.ComposedRefinementImpl#getComposedRefinements <em>Composed Refinements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComposedRefinementImpl extends AbstractRefinementImpl implements ComposedRefinement {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getAtomicRefinements() <em>Atomic Refinements</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAtomicRefinements()
     * @generated
     * @ordered
     */
    protected EList<AtomicRefinement> atomicRefinements;

    /**
     * The cached value of the '{@link #getComposedRefinements() <em>Composed Refinements</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComposedRefinements()
     * @generated
     * @ordered
     */
    protected EList<ComposedRefinement> composedRefinements;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ComposedRefinementImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RefinementsPackage.Literals.COMPOSED_REFINEMENT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.COMPOSED_REFINEMENT__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.COMPOSED_REFINEMENT__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<AtomicRefinement> getAtomicRefinements() {
        if (atomicRefinements == null) {
            atomicRefinements = new EObjectWithInverseResolvingEList.ManyInverse<AtomicRefinement>(AtomicRefinement.class, this, RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS, RefinementsPackage.ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS);
        }
        return atomicRefinements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<ComposedRefinement> getComposedRefinements() {
        if (composedRefinements == null) {
            composedRefinements = new EObjectResolvingEList<ComposedRefinement>(ComposedRefinement.class, this, RefinementsPackage.COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS);
        }
        return composedRefinements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getAtomicRefinements()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                return ((InternalEList<?>)getAtomicRefinements()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__NAME:
                return getName();
            case RefinementsPackage.COMPOSED_REFINEMENT__DESCRIPTION:
                return getDescription();
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                return getAtomicRefinements();
            case RefinementsPackage.COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS:
                return getComposedRefinements();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__NAME:
                setName((String)newValue);
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__DESCRIPTION:
                setDescription((String)newValue);
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                getAtomicRefinements().clear();
                getAtomicRefinements().addAll((Collection<? extends AtomicRefinement>)newValue);
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                getComposedRefinements().addAll((Collection<? extends ComposedRefinement>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__NAME:
                setName(NAME_EDEFAULT);
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                getAtomicRefinements().clear();
                return;
            case RefinementsPackage.COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RefinementsPackage.COMPOSED_REFINEMENT__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case RefinementsPackage.COMPOSED_REFINEMENT__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case RefinementsPackage.COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS:
                return atomicRefinements != null && !atomicRefinements.isEmpty();
            case RefinementsPackage.COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS:
                return composedRefinements != null && !composedRefinements.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", description: ");
        result.append(description);
        result.append(')');
        return result.toString();
    }

} //ComposedRefinementImpl
