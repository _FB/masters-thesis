/**
 */
package refinement.model.refinements.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import refinement.model.refinements.AbstractRefinement;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.Refinements;
import refinement.model.refinements.RefinementsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Refinements</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link refinement.model.refinements.impl.RefinementsImpl#getId <em>Id</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.RefinementsImpl#getAtomicRefinements <em>Atomic Refinements</em>}</li>
 *   <li>{@link refinement.model.refinements.impl.RefinementsImpl#getComposedRefinements <em>Composed Refinements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RefinementsImpl extends MinimalEObjectImpl.Container implements Refinements {
	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
     * The cached value of the '{@link #getAtomicRefinements() <em>Atomic Refinements</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAtomicRefinements()
     * @generated
     * @ordered
     */
    protected EList<AtomicRefinement> atomicRefinements;

    /**
     * The cached value of the '{@link #getComposedRefinements() <em>Composed Refinements</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getComposedRefinements()
     * @generated
     * @ordered
     */
    protected EList<ComposedRefinement> composedRefinements;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	protected RefinementsImpl() {
        super();
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	protected EClass eStaticClass() {
        return RefinementsPackage.Literals.REFINEMENTS;
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	public String getId() {
        return id;
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, RefinementsPackage.REFINEMENTS__ID, oldId, id));
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<AtomicRefinement> getAtomicRefinements() {
        if (atomicRefinements == null) {
            atomicRefinements = new EObjectContainmentEList<AtomicRefinement>(AtomicRefinement.class, this, RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS);
        }
        return atomicRefinements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<ComposedRefinement> getComposedRefinements() {
        if (composedRefinements == null) {
            composedRefinements = new EObjectContainmentEList<ComposedRefinement>(ComposedRefinement.class, this, RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS);
        }
        return composedRefinements;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void addRefinement(AbstractRefinement refinement) {
        if (refinement instanceof ComposedRefinement) {
            this.getComposedRefinements().add((ComposedRefinement) refinement);
        } else if (refinement instanceof AtomicRefinement) {
            this.getAtomicRefinements().add((AtomicRefinement) refinement);
        }
    }

    /**
	 * @param indexList
	 * @return
	 */
	public int getReplaceIndex(EList<Integer> indexList) {
		int replaceIndex = -1;
		for (int i = 0; i < indexList.size(); i++) {
			AtomicRefinement currentRefinement = this.getAtomicRefinements().get(indexList.get(i));
			if (currentRefinement.getKind().toUpperCase().equals("CHANGE")) {
				replaceIndex = indexList.get(i);
				indexList.remove(i);
				break;
			}
		}
		if (replaceIndex == -1) {
			replaceIndex = indexList.get(0);
			indexList.remove(0);
		}
		return replaceIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void saveRefinements(String savePath) {
	    EList<AbstractRefinement> refinements = new BasicEList<AbstractRefinement>();
	    refinements.addAll(this.getAtomicRefinements());
	    refinements.addAll(this.getComposedRefinements());
		if (!refinements.isEmpty()) {
			ResourceSet resourceSet = new ResourceSetImpl();
			Resource resource = resourceSet.createResource(URI.createFileURI(savePath));
			resource.getContents().add(this);
			try {
				resource.save(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public int indexOfRefinement(final AtomicRefinement refinement) {
        EList<AtomicRefinement> existingRefinements = this.getAtomicRefinements();
                for (int i = 0; i < existingRefinements.size(); i++) {
                    if (existingRefinements.get(i).getReferenceID().equals(refinement.getReferenceID())
                            && existingRefinements.get(i).getKind().equals(refinement.getKind())) {
                        return i;
                    }
                }
                return -1;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated NOT
     */
	public AtomicRefinement getRefinement(final int index) {
        return this.getAtomicRefinements().get(index);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated NOT
     */
    public void setRefinement(final int index, final AtomicRefinement refinement) {
        this.getAtomicRefinements().set(index, refinement);
    }


    /**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public EList<Integer> findRefinementsByReference(String referenceID) {
		EList<Integer> indexList = new BasicEList<Integer>();
		for (int i = 0; i < this.getAtomicRefinements().size(); i++) {
			AtomicRefinement currentRefinement = this.getAtomicRefinements().get(i);
			if (currentRefinement.getReferenceID().equals(referenceID)) {
				indexList.add(i);
			}
		}
		return indexList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void removeRefinement(int refinementIndex) {
		this.getAtomicRefinements().remove(refinementIndex);
	}

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS:
                return ((InternalEList<?>)getAtomicRefinements()).basicRemove(otherEnd, msgs);
            case RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS:
                return ((InternalEList<?>)getComposedRefinements()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RefinementsPackage.REFINEMENTS__ID:
                return getId();
            case RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS:
                return getAtomicRefinements();
            case RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS:
                return getComposedRefinements();
        }
        return super.eGet(featureID, resolve, coreType);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RefinementsPackage.REFINEMENTS__ID:
                setId((String)newValue);
                return;
            case RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS:
                getAtomicRefinements().clear();
                getAtomicRefinements().addAll((Collection<? extends AtomicRefinement>)newValue);
                return;
            case RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                getComposedRefinements().addAll((Collection<? extends ComposedRefinement>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public void eUnset(int featureID) {
        switch (featureID) {
            case RefinementsPackage.REFINEMENTS__ID:
                setId(ID_EDEFAULT);
                return;
            case RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS:
                getAtomicRefinements().clear();
                return;
            case RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS:
                getComposedRefinements().clear();
                return;
        }
        super.eUnset(featureID);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RefinementsPackage.REFINEMENTS__ID:
                return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
            case RefinementsPackage.REFINEMENTS__ATOMIC_REFINEMENTS:
                return atomicRefinements != null && !atomicRefinements.isEmpty();
            case RefinementsPackage.REFINEMENTS__COMPOSED_REFINEMENTS:
                return composedRefinements != null && !composedRefinements.isEmpty();
        }
        return super.eIsSet(featureID);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
        switch (operationID) {
            case RefinementsPackage.REFINEMENTS___ADD_REFINEMENT__ABSTRACTREFINEMENT:
                addRefinement((AbstractRefinement)arguments.get(0));
                return null;
            case RefinementsPackage.REFINEMENTS___SAVE_REFINEMENTS__STRING:
                saveRefinements((String)arguments.get(0));
                return null;
            case RefinementsPackage.REFINEMENTS___INDEX_OF_REFINEMENT__ATOMICREFINEMENT:
                return indexOfRefinement((AtomicRefinement)arguments.get(0));
            case RefinementsPackage.REFINEMENTS___GET_REFINEMENT__INT:
                return getRefinement((Integer)arguments.get(0));
            case RefinementsPackage.REFINEMENTS___SET_REFINEMENT__INT_ATOMICREFINEMENT:
                setRefinement((Integer)arguments.get(0), (AtomicRefinement)arguments.get(1));
                return null;
            case RefinementsPackage.REFINEMENTS___FIND_REFINEMENTS_BY_REFERENCE__STRING:
                return findRefinementsByReference((String)arguments.get(0));
            case RefinementsPackage.REFINEMENTS___REMOVE_REFINEMENT__INT:
                removeRefinement((Integer)arguments.get(0));
                return null;
            case RefinementsPackage.REFINEMENTS___GET_REPLACE_INDEX__ELIST:
                return getReplaceIndex((EList)arguments.get(0));
        }
        return super.eInvoke(operationID, arguments);
    }

	/**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * @generated
     */
	@Override
	public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuffer result = new StringBuffer(super.toString());
        result.append(" (id: ");
        result.append(id);
        result.append(')');
        return result.toString();
    }

} // RefinementsImpl
