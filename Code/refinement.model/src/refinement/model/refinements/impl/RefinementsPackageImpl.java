/**
 */
package refinement.model.refinements.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import refinement.model.refinements.AbstractRefinement;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.Refinements;
import refinement.model.refinements.RefinementsFactory;
import refinement.model.refinements.RefinementsPackage;
import refinement.model.refinements.States;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RefinementsPackageImpl extends EPackageImpl implements RefinementsPackage {
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass atomicRefinementEClass = null;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EClass refinementsEClass = null;

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass composedRefinementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass abstractRefinementEClass = null;

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private EEnum statesEEnum = null;

	/**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see refinement.model.refinements.RefinementsPackage#eNS_URI
     * @see #init()
     * @generated
     */
	private RefinementsPackageImpl() {
        super(eNS_URI, RefinementsFactory.eINSTANCE);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private static boolean isInited = false;

	/**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * 
     * <p>This method is used to initialize {@link RefinementsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
	public static RefinementsPackage init() {
        if (isInited) return (RefinementsPackage)EPackage.Registry.INSTANCE.getEPackage(RefinementsPackage.eNS_URI);

        // Obtain or create and register package
        RefinementsPackageImpl theRefinementsPackage = (RefinementsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RefinementsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RefinementsPackageImpl());

        isInited = true;

        // Initialize simple dependencies
        XMLTypePackage.eINSTANCE.eClass();

        // Create package meta-data objects
        theRefinementsPackage.createPackageContents();

        // Initialize created meta-data
        theRefinementsPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theRefinementsPackage.freeze();

  
        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(RefinementsPackage.eNS_URI, theRefinementsPackage);
        return theRefinementsPackage;
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getAtomicRefinement() {
        return atomicRefinementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_Value() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_ReferenceID() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_Difference() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_Kind() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_ValueStates() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_State() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getAtomicRefinement_ComposedRefinements() {
        return (EReference)atomicRefinementEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_ReferenceType() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAtomicRefinement_KindDetail() {
        return (EAttribute)atomicRefinementEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getAtomicRefinement__SetRefinement__String_String_String_String_String_String_String() {
        return atomicRefinementEClass.getEOperations().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getAtomicRefinement__AddValueState__String() {
        return atomicRefinementEClass.getEOperations().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getAtomicRefinement__AddValueStates__EList() {
        return atomicRefinementEClass.getEOperations().get(2);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EClass getRefinements() {
        return refinementsEClass;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EAttribute getRefinements_Id() {
        return (EAttribute)refinementsEClass.getEStructuralFeatures().get(0);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getRefinements_AtomicRefinements() {
        return (EReference)refinementsEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getRefinements_ComposedRefinements() {
        return (EReference)refinementsEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getRefinements__AddRefinement__AbstractRefinement() {
        return refinementsEClass.getEOperations().get(0);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EOperation getRefinements__SaveRefinements__String() {
        return refinementsEClass.getEOperations().get(1);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getRefinements__IndexOfRefinement__AtomicRefinement() {
        return refinementsEClass.getEOperations().get(2);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EOperation getRefinements__GetRefinement__int() {
        return refinementsEClass.getEOperations().get(3);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EOperation getRefinements__SetRefinement__int_AtomicRefinement() {
        return refinementsEClass.getEOperations().get(4);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EOperation getRefinements__FindRefinementsByReference__String() {
        return refinementsEClass.getEOperations().get(5);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EOperation getRefinements__RemoveRefinement__int() {
        return refinementsEClass.getEOperations().get(6);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EOperation getRefinements__GetReplaceIndex__EList() {
        return refinementsEClass.getEOperations().get(7);
    }

	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getComposedRefinement() {
        return composedRefinementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getComposedRefinement_Name() {
        return (EAttribute)composedRefinementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getComposedRefinement_Description() {
        return (EAttribute)composedRefinementEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getComposedRefinement_AtomicRefinements() {
        return (EReference)composedRefinementEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EReference getComposedRefinement_ComposedRefinements() {
        return (EReference)composedRefinementEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EClass getAbstractRefinement() {
        return abstractRefinementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EAttribute getAbstractRefinement_Date() {
        return (EAttribute)abstractRefinementEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public EEnum getStates() {
        return statesEEnum;
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public RefinementsFactory getRefinementsFactory() {
        return (RefinementsFactory)getEFactoryInstance();
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isCreated = false;

	/**
     * Creates the meta-model objects for the package.  This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        atomicRefinementEClass = createEClass(ATOMIC_REFINEMENT);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__VALUE);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__REFERENCE_ID);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__DIFFERENCE);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__KIND);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__VALUE_STATES);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__STATE);
        createEReference(atomicRefinementEClass, ATOMIC_REFINEMENT__COMPOSED_REFINEMENTS);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__REFERENCE_TYPE);
        createEAttribute(atomicRefinementEClass, ATOMIC_REFINEMENT__KIND_DETAIL);
        createEOperation(atomicRefinementEClass, ATOMIC_REFINEMENT___SET_REFINEMENT__STRING_STRING_STRING_STRING_STRING_STRING_STRING);
        createEOperation(atomicRefinementEClass, ATOMIC_REFINEMENT___ADD_VALUE_STATE__STRING);
        createEOperation(atomicRefinementEClass, ATOMIC_REFINEMENT___ADD_VALUE_STATES__ELIST);

        refinementsEClass = createEClass(REFINEMENTS);
        createEAttribute(refinementsEClass, REFINEMENTS__ID);
        createEReference(refinementsEClass, REFINEMENTS__ATOMIC_REFINEMENTS);
        createEReference(refinementsEClass, REFINEMENTS__COMPOSED_REFINEMENTS);
        createEOperation(refinementsEClass, REFINEMENTS___ADD_REFINEMENT__ABSTRACTREFINEMENT);
        createEOperation(refinementsEClass, REFINEMENTS___SAVE_REFINEMENTS__STRING);
        createEOperation(refinementsEClass, REFINEMENTS___INDEX_OF_REFINEMENT__ATOMICREFINEMENT);
        createEOperation(refinementsEClass, REFINEMENTS___GET_REFINEMENT__INT);
        createEOperation(refinementsEClass, REFINEMENTS___SET_REFINEMENT__INT_ATOMICREFINEMENT);
        createEOperation(refinementsEClass, REFINEMENTS___FIND_REFINEMENTS_BY_REFERENCE__STRING);
        createEOperation(refinementsEClass, REFINEMENTS___REMOVE_REFINEMENT__INT);
        createEOperation(refinementsEClass, REFINEMENTS___GET_REPLACE_INDEX__ELIST);

        composedRefinementEClass = createEClass(COMPOSED_REFINEMENT);
        createEAttribute(composedRefinementEClass, COMPOSED_REFINEMENT__NAME);
        createEAttribute(composedRefinementEClass, COMPOSED_REFINEMENT__DESCRIPTION);
        createEReference(composedRefinementEClass, COMPOSED_REFINEMENT__ATOMIC_REFINEMENTS);
        createEReference(composedRefinementEClass, COMPOSED_REFINEMENT__COMPOSED_REFINEMENTS);

        abstractRefinementEClass = createEClass(ABSTRACT_REFINEMENT);
        createEAttribute(abstractRefinementEClass, ABSTRACT_REFINEMENT__DATE);

        // Create enums
        statesEEnum = createEEnum(STATES);
    }

	/**
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	private boolean isInitialized = false;

	/**
     * Complete the initialization of the package and its meta-model.  This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        atomicRefinementEClass.getESuperTypes().add(this.getAbstractRefinement());
        composedRefinementEClass.getESuperTypes().add(this.getAbstractRefinement());

        // Initialize classes, features, and operations; add parameters
        initEClass(atomicRefinementEClass, AtomicRefinement.class, "AtomicRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAtomicRefinement_Value(), ecorePackage.getEString(), "value", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_ReferenceID(), ecorePackage.getEString(), "referenceID", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_Difference(), ecorePackage.getEString(), "difference", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_Kind(), ecorePackage.getEString(), "kind", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_ValueStates(), ecorePackage.getEString(), "valueStates", null, 0, -1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_State(), this.getStates(), "state", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAtomicRefinement_ComposedRefinements(), this.getComposedRefinement(), this.getComposedRefinement_AtomicRefinements(), "composedRefinements", null, 0, -1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_ReferenceType(), ecorePackage.getEString(), "referenceType", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAtomicRefinement_KindDetail(), ecorePackage.getEString(), "kindDetail", null, 0, 1, AtomicRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        EOperation op = initEOperation(getAtomicRefinement__SetRefinement__String_String_String_String_String_String_String(), null, "setRefinement", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "reference", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "difference", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "kind", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "valueState", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "referenceType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "kindDetail", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getAtomicRefinement__AddValueState__String(), null, "addValueState", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getAtomicRefinement__AddValueStates__EList(), null, "addValueStates", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEEList(), "states", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(refinementsEClass, Refinements.class, "Refinements", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getRefinements_Id(), ecorePackage.getEString(), "id", null, 0, 1, Refinements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getRefinements_AtomicRefinements(), this.getAtomicRefinement(), null, "atomicRefinements", null, 0, -1, Refinements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getRefinements_ComposedRefinements(), this.getComposedRefinement(), null, "composedRefinements", null, 0, -1, Refinements.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = initEOperation(getRefinements__AddRefinement__AbstractRefinement(), null, "addRefinement", 0, 1, !IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, this.getAbstractRefinement(), "refinement", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__SaveRefinements__String(), null, "saveRefinements", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "savePath", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__IndexOfRefinement__AtomicRefinement(), ecorePackage.getEInt(), "indexOfRefinement", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, this.getAtomicRefinement(), "refinement", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__GetRefinement__int(), this.getAtomicRefinement(), "getRefinement", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEInt(), "index", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__SetRefinement__int_AtomicRefinement(), null, "setRefinement", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEInt(), "index", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, this.getAtomicRefinement(), "refinement", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__FindRefinementsByReference__String(), ecorePackage.getEEList(), "findRefinementsByReference", 0, 1, IS_UNIQUE, !IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "referenceID", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__RemoveRefinement__int(), null, "removeRefinement", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEInt(), "refinementIndex", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = initEOperation(getRefinements__GetReplaceIndex__EList(), theXMLTypePackage.getInt(), "getReplaceIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEEList(), "indexList", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(composedRefinementEClass, ComposedRefinement.class, "ComposedRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getComposedRefinement_Name(), ecorePackage.getEString(), "name", null, 0, 1, ComposedRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getComposedRefinement_Description(), ecorePackage.getEString(), "description", null, 0, 1, ComposedRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getComposedRefinement_AtomicRefinements(), this.getAtomicRefinement(), this.getAtomicRefinement_ComposedRefinements(), "atomicRefinements", null, 0, -1, ComposedRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getComposedRefinement_ComposedRefinements(), this.getComposedRefinement(), null, "composedRefinements", null, 0, -1, ComposedRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(abstractRefinementEClass, AbstractRefinement.class, "AbstractRefinement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractRefinement_Date(), ecorePackage.getEDate(), "date", null, 0, 1, AbstractRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(statesEEnum, States.class, "States");
        addEEnumLiteral(statesEEnum, States.RESOLVED);
        addEEnumLiteral(statesEEnum, States.TODO);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations();
    }

	/**
     * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
     * @generated
     */
	protected void createExtendedMetaDataAnnotations() {
        String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";	
        addAnnotation
          (atomicRefinementEClass, 
           source, 
           new String[] {
             "kind", "mixed"
           });
    }

} //RefinementsPackageImpl
