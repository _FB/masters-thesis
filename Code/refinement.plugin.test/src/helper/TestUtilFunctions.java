package helper;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import utils.RefinementConstants;
import utils.CommonUtils;

/**
 * Util functions for testing purpose.
 * 
 * @author frborn
 *
 */
public final class TestUtilFunctions {
    /**
     * Private constructor to prevent instantiation
     */
    private TestUtilFunctions() {
        throw new UnsupportedOperationException();
    }

    /**
     * Create HashMap for "pcm", "refinement" and "extracted" folder as iFolder.
     * 
     * @param iProject
     *            test project
     * @return HashMap with keys: "pcm", "refinement", "extracted"
     */
    public static Map<String, IFolder> getFolders(IProject iProject) {
        final Map<String, IFolder> folders = new HashMap<String, IFolder>();
        folders.put(RefinementConstants.PCM.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.PCM.getName()));
        folders.put(RefinementConstants.REFINEMENT.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.REFINEMENT.getName()));
        folders.put(RefinementConstants.EXTRACTED.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.EXTRACTED.getName()));
        return folders;
    }

    /**
     * Load Comparison Object from IFile
     * 
     * @param diffFile
     *            IFile that contains Comparison Object
     * @return Comparison Object
     */
    public static Comparison loadDiffs(final IFile diffFile) {
        final ResourceSet resourceSet = new ResourceSetImpl();
        Resource xmiResource;
        try {
            xmiResource = resourceSet.getResource(URI.createFileURI(diffFile.getFullPath().toString()), true);
            xmiResource.load(null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Comparison comparison = (Comparison) xmiResource.getContents().get(0);
        return comparison;
    }
}
