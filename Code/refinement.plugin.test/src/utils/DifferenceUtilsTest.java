package utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Test;
import org.junit.tools.configuration.base.MethodRef;

import helper.TestUtilFunctions;

@Generated(value = "org.junit-tools-1.1.0")
public class DifferenceUtilsTest {
    private final IWorkspaceRoot currentWorkspace = ResourcesPlugin.getWorkspace().getRoot();
    private final IProject project = this.currentWorkspace.getProject("differenceUtil");
    private Map<String, IFolder> folders = TestUtilFunctions.getFolders(this.project);

    private final IFile diffFile = this.project.getFile("diffModel.comparison");
    private final Comparison comparison = TestUtilFunctions.loadDiffs(diffFile);

    @MethodRef(name = "extractValue", signature = "(QDiff;)QString;")
    @Test
    public void testExtractValue() throws Exception {
        Diff diff = this.comparison.getDifferences().get(0);
        String result;

        // default test
        result = DifferenceUtils.extractValue(diff);
        assertTrue(
                "Value 'Provided_ITechnicalDataProvider-TechnicalDataProvider_EXTRACTED2.2' was extracted succesfully",
                result.equals("Provided_ITechnicalDataProvider-TechnicalDataProvider_EXTRACTED2.2"));
    }

    @MethodRef(name = "compareResources", signature = "(QResourceSet;QResourceSet;)QComparison;")
    @Test
    public void testCompareResources() throws Exception {
        IFile pcmRepository = this.folders.get(RefinementConstants.PCM.getName()).getFile("extracted.repository");
        IFile extractedRepository = this.folders.get(RefinementConstants.EXTRACTED.getName())
                .getFile("extracted.repository");
        ResourceSet latestResourceSet = new ResourceSetImpl();
        Resource latestResource = latestResourceSet
                .getResource(URI.createFileURI(pcmRepository.getFullPath().toString()), true);
        ResourceSet extractedResourceSet = new ResourceSetImpl();
        extractedResourceSet.getResource(URI.createFileURI(extractedRepository.getFullPath().toString()), true);
        Comparison result;
        // default test
        result = DifferenceUtils.compareResources(latestResourceSet, extractedResourceSet);
        assertTrue("Differences are equal to reference diffModel.",
                result.getDifferences().toString().equals(this.comparison.getDifferences().toString()));
    }

    @MethodRef(name = "extractKindDetail", signature = "(QDiff;)QString;")
    @Test
    public void testExtractKindDetail() throws Exception {
        Diff diff = this.comparison.getDifferences().get(0);
        String result;

        // default test
        result = DifferenceUtils.extractKindDetail(diff);
        assertTrue("Kind: 'CHANGE entityName' was extracted sucessfully", result.equals("entityName"));
    }

    @MethodRef(name = "extractStructuralFeature", signature = "(QDiff;QString;)QString;")
    @Test
    public void testExtractStructuralFeature() throws Exception {
        Diff diff = this.comparison.getDifferences().get(0);
        String feature = "id";
        String result;
        // default test
        result = DifferenceUtils.extractStructuralFeature(diff, feature);
        assertTrue("Change Diff can't be used to extract StructuralFeature", true);
    }

    @MethodRef(name = "reverseDiffs", signature = "(QComparison;)QList<QDiff;>;")
    @Test
    public void testReverseDiffs() throws Exception {
        List<Diff> originOrderedList = this.comparison.getDifferences();
        List<Diff> result;
        // Compare originOrderedList starting with highest index
        // with reversed List<Diff>
        result = DifferenceUtils.reverseDiffs(this.comparison);
        int j = 0;
        for (int i = originOrderedList.size() - 1; i >= 0; i--) {
            if (!originOrderedList.get(i).equals(result.get(j))) {
                assertFalse("Reverse Failed", false);
            }
            j++;
        }
        assertTrue("Reverse success", true);
    }

    @MethodRef(name = "extractElementType", signature = "(QDiff;)QString;")
    @Test
    public void testExtractElementType() throws Exception {
        Diff diff = this.comparison.getDifferences().get(0);
        String result;
        String expectedResult = "class org.eclipse.emf.ecore.impl.EObjectImpl";
        // default test
        result = DifferenceUtils.extractElementType(diff);
        assertTrue("Expected result!", expectedResult.equals(result));
    }
}