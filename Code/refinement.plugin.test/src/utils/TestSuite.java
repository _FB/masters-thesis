package utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(

{DifferenceUtilsTest.class, RenameBasicComponent.class}
)
public class TestSuite{ // nothing
}
