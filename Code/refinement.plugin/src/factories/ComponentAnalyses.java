package factories;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * Identifier for factory pattern of static component analysis
 * 
 * @author frborn
 *
 */
public enum ComponentAnalyses implements Enumerator {
    /**
     * The '<em><b>GCC</b></em>' literal object. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #GCC_VALUE
     * @ordered
     */
    GCC(1, "GCC", "GCC");

    /**
     * The '<em><b>GCC_VALUE</b></em>' literal value. <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>GCC_VALUE</b></em>' literal object isn't clear, there really should
     * be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #GCC
     * @ordered
     */
    public static final int GCC_VALUE = 1;

    /**
     * An array of all the '<em><b>ComponentAnalyses</b></em>' enumerators. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     */
    private static final ComponentAnalyses[] VALUES_ARRAY = new ComponentAnalyses[] { GCC, };

    /**
     * A public read-only list of all the '<em><b>States</b></em>' enumerators. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     */
    public static final List<ComponentAnalyses> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified literal value. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param literal
     *            the literal.
     * @return the matching enumerator or <code>null</code>.
     */
    public static ComponentAnalyses get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ComponentAnalyses result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified name. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name
     *            the name.
     * @return the matching enumerator or <code>null</code>.
     */
    public static ComponentAnalyses getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ComponentAnalyses result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified integer value. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the integer value.
     * @return the matching enumerator or <code>null</code>.
     */
    public static ComponentAnalyses get(int value) {
        switch (value) {
        case GCC_VALUE:
            return GCC;
        default:
            break;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private ComponentAnalyses(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }
}
