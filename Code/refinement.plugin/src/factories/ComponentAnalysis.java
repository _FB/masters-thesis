package factories;

import org.eclipse.core.commands.ExecutionException;

/**
 * Interface for static code analysis.
 * 
 * @author frborn
 *
 */
public interface ComponentAnalysis {
    /**
     * Execute static code analysis
     * 
     * @throws ExecutionException if execution fails
     */
    public void execute() throws ExecutionException;
}
