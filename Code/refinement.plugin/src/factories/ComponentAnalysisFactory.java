/**
 * This Package contains a factory for creating component analysis strategies
 */
package factories;

/**
 * Factory for instancing static code analysis implementation. If a new logic is added to
 * "src/factories/*" the identifier must be added to "StaticAnalysisFactory()".
 * 
 * @author frborn
 *
 */
public class ComponentAnalysisFactory {
    /**
     * 
     * @param analysisType
     *            String for choosing constructor
     * @return instance of static code analysis
     */
    public static ComponentAnalysis getStaticAnalysis(String analysisType) {
        if (analysisType == null) {
            return null;
        }
        if (analysisType.equalsIgnoreCase(ComponentAnalyses.GCC.getName())) {
            return new GccComponentAnalysis();
        }
        return null;
    }
}
