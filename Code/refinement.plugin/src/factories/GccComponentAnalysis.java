package factories;

import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.palladiosimulator.pcm.repository.OperationInterface;
import org.palladiosimulator.pcm.repository.Repository;
import org.palladiosimulator.pcm.repository.RepositoryComponent;
import org.palladiosimulator.pcm.repository.RepositoryFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import utils.LoggingUtils;
import utils.RefinementConstants;
import utils.CommonUtils;
import utils.XMLUtils;

/**
 * Implementation of GCC specific static code analysis using component.xml file for identifying
 * potential components. The GCC static analysis will be created by calling
 * StaticAnalaysisFactory.getStaticAnalysis(event, "GCC").
 * 
 * @author frborn
 *
 */
public class GccComponentAnalysis implements ComponentAnalysis {

    private ExecutionEvent event;

    /**
     * 
     */
    public GccComponentAnalysis() {
    }

    @Override
    public void execute() throws ExecutionException {
        // Load selected project and mandatory folder
        final Map<String, IFolder> folders = CommonUtils.getFolders();

        // Get repository, corresponding refinement and kk_service_table.xml
        IFile repositoryFile = CommonUtils.getCorrespondingFile(folders.get(RefinementConstants.PCM.getName()),
                RefinementConstants.REPOSITORY.getName(), 1);
        IFile refinementFile = CommonUtils.getCorrespondingFile(folders.get(RefinementConstants.REFINEMENT.getName()),
                RefinementConstants.REPOSITORY.getName(), 1);
        IFile serviceTableFile = folders.get(RefinementConstants.COMPONENT_ANALYSIS.getName())
                .getFile("kk_service_table.xml");

        // List for saving the names of potential new components
        EList<String> potentialComponents = new BasicEList<String>();

        // Load repository from repository file and get all components
        Repository repository = CommonUtils.loadRepository(repositoryFile.getFullPath().toString());
        EList<RepositoryComponent> components = repository.getComponents__Repository();

        LoggingUtils.logging("Start static code analysis.");
        // Check kk_service_table.xml file for components that are not part of
        // repository and component name to potentialComponents.
        this.findComponentsForServiceTable(repositoryFile, refinementFile, potentialComponents, serviceTableFile,
                repository, components);

        // Check if all folder names of source folder are represented in the
        // repository file and folder name to potentialComponents
        this.findComponentsForProjectFolder(folders.get(RefinementConstants.COMPONENT_ANALYSIS.getName()),
                repositoryFile, refinementFile, potentialComponents);

        // Open ListSelectionDialog with all potential components preselected.
        // Add all selected names to selectedComponents after confirmation.
        EList<String> selectedComponents = CommonUtils.openStringSelectionDialog(potentialComponents, "Missing components",
                "Select Components to create:");

        // Create BasicComponents and OperationInterface for every name in
        // selectedComponents
        this.addComponents(repositoryFile, selectedComponents, repository);
        LoggingUtils.componentSucess(selectedComponents);
    }

    /**
     * Find Check if value of attribute "name" of all "service" elements in "kk_service_table.xml"
     * exist as entityName for any BasicComponent in Repository or if it was an entityName at any
     * time before that BasicComponent was manually refined. If nothing is found, component name
     * will be added to potentialComponents
     * 
     * @param repositoryFile
     *            *.repository IFile
     * @param refinementFile
     *            *.refinements IFile
     * @param createdComponents
     *            List of component names
     * @param serviceTableFile
     *            serviceTable.xml IFile
     * @param repository
     *            Repository instance
     * @param components
     *            List of RepositoryComponents
     */
    private void findComponentsForServiceTable(IFile repositoryFile, IFile refinementFile,
            EList<String> createdComponents, IFile serviceTableFile, Repository repository,
            EList<RepositoryComponent> components) {
        if (serviceTableFile.exists()) {
            // Get all "service" elements from kk_service_table.xml file
            Document serviceTable = XMLUtils.getXMLTree(serviceTableFile);
            List<Node> services = XMLUtils.getElementsByTagName((Node) serviceTable, "service");
            // Find potential new component names by looking at repository and
            // refinement file and add component names to potentialComponents
            for (Node service : services) {
                String componentName = CommonUtils.extractComponentName(service);
                this.findComponent(repositoryFile, refinementFile, createdComponents, repository, components,
                        componentName);
            }
        }
    }

    /**
     * Check if name of direct sub folder of componentFolder exist as entityName of any
     * BasicComponent in Repository and or if it was an entityName at any time before that
     * BasicComponent was manually refined. If nothing is found, component name will be added to
     * potentialComponents
     * 
     * @param componentFolder
     *            IFolder of source code
     * @param repositoryFile
     *            IFile of *.repository
     * @param refinementFile
     *            IFile of *.refinement
     * @param createdComponents
     *            EList of component names that should be created
     */
    private void findComponentsForProjectFolder(final IFolder componentFolder, IFile repositoryFile,
            IFile refinementFile, EList<String> createdComponents) {
        for (String componentName : CommonUtils.getComponentNames(componentFolder)) {
            // Reload repository from repository file and get all components
            Repository repository = CommonUtils.loadRepository(repositoryFile.getFullPath().toString());
            EList<RepositoryComponent> components = repository.getComponents__Repository();
            this.findComponent(repositoryFile, refinementFile, createdComponents, repository, components,
                    componentName);
        }
    }

    /**
     * Check if given component name exists as entityName for a current BasicComponent in repository
     * file or was ever used as an entityName and was overwritten by a manual refinement. If no such
     * BasicComponent was found the component name is added to potentialComponents.
     * 
     * @param repositoryFile
     *            IFile *.repository
     * @param refinementFile
     *            IFile *.refinement
     * @param potentialComponents
     *            EList names of potential undetected components
     * @param repository
     *            Repository
     * @param components
     *            EList of components
     * @param componentName
     *            name of component
     */
    private void findComponent(IFile repositoryFile, IFile refinementFile, EList<String> potentialComponents,
            Repository repository, EList<RepositoryComponent> components, String componentName) {
        if (refinementFile != null) {
            // Check index of BasicComponent in repository for componentName
            if (CommonUtils.repositoryIndexOfComponent(components, componentName) == -1) {
                // Check index of refinement with "valueStates" that contains
                // given component name
                if (CommonUtils.refinementIndexOfComponent(refinementFile, componentName) == -1) {
                    // Add component name to createdComponents if the list
                    // does not contain any form of component name
                    if (CommonUtils.repositoryIndexOfCreatedComponent(potentialComponents, componentName) == -1) {
                        potentialComponents.add(componentName);
                    }
                }
            }
        } else {
            // Check index of BasicComponent in repository for componentName
            if (CommonUtils.repositoryIndexOfComponent(components, componentName) == -1) {
                // Add component name to createdComponents if the list
                // does not contain any form of component name
                if (CommonUtils.repositoryIndexOfCreatedComponent(potentialComponents, componentName) == -1) {
                    potentialComponents.add(componentName);
                }
            }
        }
    }

    /**
     * Add BasicComponent and OperationInterface for every componentName in selectedComponents to
     * repository.
     * 
     * @param repositoryFile
     *            IFile *.repository
     * @param selectedComponents
     *            EList names of selected components
     * @param repository
     *            Repository file
     */
    public void addComponents(IFile repositoryFile, EList<String> selectedComponents, Repository repository) {
        for (String componentName : selectedComponents) {
            // Create and add component
            RepositoryComponent newComponent = RepositoryFactory.eINSTANCE.createBasicComponent();
            newComponent.setEntityName(componentName);
            newComponent.setRepository__RepositoryComponent(repository);

            // Create and add corresponding Interface
            OperationInterface newInterface = RepositoryFactory.eINSTANCE.createOperationInterface();
            newInterface.setEntityName("I" + componentName);
            newInterface.setRepository__Interface(repository);
        }
        CommonUtils.saveRepository(repositoryFile, repository);
    }

}
