/**
 * Package that implements label provider emf.compare.Diff
 */
package provider;

import org.eclipse.emf.compare.Diff;
import org.eclipse.jface.viewers.LabelProvider;

import utils.DifferenceUtils;

public class DiffLabelProvider extends LabelProvider {
    @Override
    public String getText(Object object) {
        String label = "";
        Diff diff = (Diff) object;
        String diffValue = DifferenceUtils.extractValue(diff);
        String elementType = "";
        String[] elementTypeClass = DifferenceUtils.extractElementType(diff).toString().trim().split("\\.");
        if (elementTypeClass.length > 0) {
            elementType = elementTypeClass[elementTypeClass.length - 1];
        }

        String kindDetail = DifferenceUtils.extractKindDetail(diff);
        String kind = diff.getKind().toString();
        String source = diff.getSource().toString();
        label = kind + " " + kindDetail + ": " + elementType + " '" + diffValue + "' in source '" + source + "'";
        return label;
    }

}
