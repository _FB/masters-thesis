/**
 * Package that implements label provider for PCM file names
 */
package provider;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.LabelProvider;

/**
 * LabelProvider for displaying PCM IFile in ListSelectionDialog.
 * 
 * @author frborn
 *
 */
public class PCMFileProvider extends LabelProvider {
    @Override
    public String getText(Object object) {
        return ((IFile) object).getName();
    }
}
