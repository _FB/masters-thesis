package provider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.jface.viewers.LabelProvider;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.States;

/**
 * LabelProvider for displaying Refinement in ListSelectionDialog.
 * 
 * @author frborn
 *
 */
public class RefinementLabelProvider extends LabelProvider {
    @Override
    public String getText(Object object) {
        String label = "";
        if (object instanceof AtomicRefinement) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            States state = ((AtomicRefinement) object).getState();
            label = ((AtomicRefinement) object).getKind() + " " + ((AtomicRefinement) object).getKindDetail() 
                    + ": '" + ((AtomicRefinement) object).getValue() + "' [ReferenceID:  " 
                    + ((AtomicRefinement) object).getReferenceID() + "] ";
            Date date = ((AtomicRefinement) object).getDate();
            if (date != null) {
                label = label + "[Created: " + dateFormat.format(date) + "]";
            }
            if (state != null) {
                if (state.equals(States.TODO)) {
                    label = "[State: " + state + "] " + label;
                }
            }
        } else if (object instanceof ComposedRefinement) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            label = ((ComposedRefinement) object).getName();
            Date date = ((ComposedRefinement) object).getDate();
            if (date != null) {
                label = label + " [Created: " + dateFormat.format(date) + "]";
            }
        }
        return label;
    }
}
