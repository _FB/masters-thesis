package provider;

import java.util.Collection;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ITreeContentProvider;

import refinement.model.refinements.AbstractRefinement;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.Refinements;

/**
 * Content Provider for displaying implementations of AbstractRefinements in a tree viewer
 * 
 * @author frborn
 *
 */
public class TreeContentProvider implements ITreeContentProvider {
    @Override
    public boolean hasChildren(Object element) {
        Object[] obj = getChildren(element);
        return obj == null ? false : obj.length > 0;
    }

    @Override
    public Object getParent(Object element) {
        if (element instanceof AtomicRefinement) {
            ComposedRefinement parent = ((AtomicRefinement) element).getComposedRefinements().get(0);
            return parent;
        }
        return null;
    }

    @Override
    public Object[] getElements(Object inputElement) {
        EList<AbstractRefinement> refinementContent = new BasicEList<AbstractRefinement>();
        if (inputElement instanceof ComposedRefinement) {
            refinementContent.addAll(((ComposedRefinement) inputElement).getAtomicRefinements());
            return refinementContent.toArray();
        } else if (inputElement instanceof AtomicRefinement) {
            refinementContent.add((AtomicRefinement) inputElement);
            return refinementContent.toArray();
        } else if (inputElement instanceof Refinements) {
            EList<AbstractRefinement> newObj = new BasicEList<AbstractRefinement>();
            newObj.addAll(((Refinements) inputElement).getComposedRefinements());
            return refinementContent.toArray();
        } else if (inputElement instanceof EList<?>) {
            refinementContent.addAll((Collection<? extends AbstractRefinement>) inputElement);
            return refinementContent.toArray();
        }
        return refinementContent.toArray();
    }

    @Override
    public Object[] getChildren(Object parentElement) {
        if (parentElement instanceof ComposedRefinement) {
            Object[] test = ((ComposedRefinement) parentElement).getAtomicRefinements().toArray();
            return test;
        }
        return null;
    }
}