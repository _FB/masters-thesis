/**
 * Package that implements factory pattern for creating static code analysis strategies
 */
package refinement.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import factories.ComponentAnalyses;
import factories.ComponentAnalysis;
import factories.ComponentAnalysisFactory;
import utils.LoggingUtils;
import utils.CommonUtils;

/**
 * Handler that adds information from static code analysis to PCM repository file. It creates new
 * BasicComponent and OperationInterface and adds them to repository.
 * 
 * @author frborn
 * 
 */
public class ComponentHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        EList<String> availableFactories = getAvailableFactories();
        EList<String> selectedFactories = CommonUtils.openStringSelectionDialog(availableFactories, "Available Stategies",
                "Select strategies for static code analysis:");
        if (selectedFactories.isEmpty()) {
            LoggingUtils.showNotification(
                    "Info: No static code analysis implementation was selected. Static code analysis was terminated!");
        } else {
            // Execute static code analysis for every selected component analysis factory
            for (String factory : selectedFactories) {
                ComponentAnalysis componentAnalysis = ComponentAnalysisFactory.getStaticAnalysis(factory);
                componentAnalysis.execute();
            }
        }

        return null;
    }

    /**
     * Return all available factories that implement different static code analysis. If new
     * factories are created in src/factories/* the new factory identifier must be added here.
     * 
     * @return List of all available factories as string
     */
    public EList<String> getAvailableFactories() {
        EList<String> availableFactories = new BasicEList<String>();
        // Add new factory identifier here:
        availableFactories.add(ComponentAnalyses.GCC.getName());
        return availableFactories;
    }

}
