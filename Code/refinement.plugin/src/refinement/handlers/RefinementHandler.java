package refinement.handlers;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.merge.IMerger;
import org.eclipse.emf.compare.merge.IMerger.RegistryImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.ui.dialogs.ListSelectionDialog;

import provider.DiffLabelProvider;
import provider.PCMFileProvider;
import refinement.model.refinements.Refinements;
import refinement.model.refinements.RefinementsPackage;
import utils.DifferenceUtils;
import utils.LoggingUtils;
import utils.RefinementConstants;
import utils.CommonUtils;

/**
 * Handler that merges differences from extracted PCM files to actual PCM files. Only differences
 * that don't overwrite manual architectural changes are merged to actual PCM files.
 * 
 * @author frborn
 *
 */
public class RefinementHandler extends AbstractHandler {
    private IWorkbenchWindow window = CommonUtils.getWindow();
    private EList<String> modifiedFiles = new BasicEList<String>();
    private final Map<String, IFolder> folders = CommonUtils.getFolders();
    private EList<IFile> selectedFiles = new BasicEList<IFile>();
    private EList<Diff> detectedDifferences = new BasicEList<Diff>();
    private EList<Diff> selectedDifferences = new BasicEList<Diff>();
    private EList<Diff> unmatchedDifferences = new BasicEList<Diff>();
    private Resource latestResource = null;

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        openPCMSelectionDialog();
        LoggingUtils.logging("Starting automatic refinement");
        for (IFile pcmFile : this.selectedFiles) {
            // Initialize PCM and refinement file
            final IFile extractedFile = CommonUtils.getCorrespondingFile(
                    this.folders.get(RefinementConstants.EXTRACTED.getName()), CommonUtils.getFileName(pcmFile), 1);
            /**
             * Find all Differences that can not be matched to a refinement and add to
             * this.unmatchedDifferences
             */
            findUnmatchedDifferences(pcmFile, extractedFile);
            openSavingDifferenceDialog(pcmFile.getName());
        }
        LoggingUtils.refinementSucess(this.modifiedFiles);
        this.modifiedFiles.clear();
        this.selectedFiles.clear();
        return null;
    }

    /**
     * Load refinements for current PCM file. Execute EMF compare and check if differences affect
     * components that were manually refined. Those differences will not be merged, all other
     * differences are merged to PCM component.
     * 
     * @param pcmFile
     *            IFile of current PCM model
     * @param extractedFile
     *            IFile of corresponding extracted PCM model
     */
    private void findUnmatchedDifferences(IFile pcmFile, final IFile extractedFile) {
        ResourceSet latestResourceSet = new ResourceSetImpl();
        this.latestResource = latestResourceSet.getResource(URI.createFileURI(pcmFile.getFullPath().toString()), true);
        ResourceSet extractedResourceSet = new ResourceSetImpl();
        extractedResourceSet.getResource(URI.createFileURI(extractedFile.getFullPath().toString()), true);
        Comparison diffModel = DifferenceUtils.compareResources(latestResourceSet, extractedResourceSet);
        // Check refinement file exists
        String refinementFileName = pcmFile.getName() + "." + RefinementsPackage.eNAME;
        boolean refinementFileExists = CommonUtils
                .fileExists(this.folders.get(RefinementConstants.REFINEMENT.getName()), refinementFileName);
        if (refinementFileExists) {
            /*
             * Case 1: Refinement steps for currentPCMFileName are available under
             * refinement.{currentPCMFileName} Check if difference of extracted would overwrite a
             * manual refinement step: If no refinement step can be matched to Difference add
             * Difference to this.unmatchedDifferences
             */
            addUnmatchedDifferences(pcmFile, diffModel, refinementFileName);
        } else {
            /*
             * Case 2: No manual refinement steps are saved in refinement.{currentPCMFileName}.
             * Because there are no manual refinement steps for the current PCM file available, all
             * detected Differences are saved in this.detectedDifferences, all detected Differences
             * saved in this.unmatchedDifferences and are therefore preselected in the
             * openSavingDifferenceDialog
             */
           
            addAllDifferencesAsUnmatched(pcmFile, diffModel);
        }
    }

    /**
     * No Differences can be matched, because there are no refinement steps available
     * 
     * @param pcmFile
     *            current PCM file
     * @param diffModel
     *            current Comparison
     */
    private void addAllDifferencesAsUnmatched(IFile pcmFile, Comparison diffModel) {
        if (!diffModel.getDifferences().isEmpty()) {
            this.detectedDifferences.addAll(diffModel.getDifferences());
            this.unmatchedDifferences.addAll(diffModel.getDifferences());
        } else {
            LoggingUtils.logging("Warning: No differences detected for \"base." + pcmFile.getName() + "\".");
        }
    }

    /**
     * Find refinement that matches all Differences of the Comparison diffModel
     * 
     * @param pcmFile
     *            current PCM file
     * @param diffModel
     *            current Comparison
     * @param refinementFileName
     *            name of the refinement file
     */
    private void addUnmatchedDifferences(IFile pcmFile, Comparison diffModel, String refinementFileName) {
        IFile refinementFile = this.folders.get(RefinementConstants.REFINEMENT.getName()).getFile(refinementFileName);
        Refinements oldRefinements = CommonUtils.loadRefinements(refinementFile);
        /*
         * Check if differences would affect an element that was manually refined. Only merge
         * difference if no refinement step was saved in {PCM}.refinements
         */
        for (Diff diff : diffModel.getDifferences()) {
            this.detectedDifferences.add(diff);
            String referenceID = DifferenceUtils.extractStructuralFeature(diff, "id");
            if (referenceID != null) {
                // Check if difference has corresponding refinement
                int differenceIndex = DifferenceUtils.refinementIndexOfDifference(oldRefinements, diff);
                if (differenceIndex > -1) {
                    LoggingUtils.logging("Warning: Manual refinement for element (" + referenceID
                            + ") exists. Automatic refinement will be skipped for this element");
                } else {
                    // Add current PCM file to this.modifiedFiles
                    if (!this.modifiedFiles.contains(pcmFile.getName())) {
                        // Add current PCM file for success log
                        this.modifiedFiles.add(pcmFile.getName());
                    }
                    // Add difference to this.unmatchedDifferences
                    this.unmatchedDifferences.add(diff);
                }
            }
        }
    }

    /**
     * Load all PCM IFiles as selection options in ListSelectionDialog. Execute automated refinement
     * for selected IFiles.
     * 
     */
    public void openPCMSelectionDialog() {
        String selectionName = "PCM files";
        ListSelectionDialog dialog = createPCMSelectionDialog(selectionName,
                CommonUtils.getAllIFiles(this.folders.get(RefinementConstants.PCM.getName())));
        if (dialog.open() == ListSelectionDialog.OK) {
            Object[] result = dialog.getResult();
            for (Object selected : result) {
                this.selectedFiles.add((IFile) selected);
            }
        }
    }

    /**
     * Create ListSelectionDialog that contains PCM IFiles as selection options.
     * 
     * @param selectionName
     *            name of ListSelectionDialog
     * @param pcmFiles
     *            EList of IFiles
     * @return ListSelectionDialog
     */
    public ListSelectionDialog createPCMSelectionDialog(String selectionName, EList<IFile> pcmFiles) {
        ListSelectionDialog dialog = new ListSelectionDialog(this.window.getShell(), pcmFiles.toArray(),
                new ArrayContentProvider(), new PCMFileProvider(), "Select PCM files:");
        dialog.setTitle("Refinement Selection: " + selectionName);
        dialog.setInitialSelections(pcmFiles.toArray());
        return dialog;
    }

    /**
     * Load all refinements as selection options in ListSelectionDialog. Add selected Refinement to
     * refinements and add current PCM file to modifiedFiles.
     * 
     * @param fileName
     *            name of current PCM file
     */
    private void openSavingDifferenceDialog(String fileName) {
        ListSelectionDialog diffSelectionDialog = createDiffSelectionDialog(fileName);
        if (diffSelectionDialog.open() == CheckedTreeSelectionDialog.OK) {
            Object[] result = diffSelectionDialog.getResult();
            for (Object selected : result) {
                this.selectedDifferences.add((Diff) selected);
            }
            if (result.length > 0) {
                for (Diff diff : this.selectedDifferences) {
                    IMerger.Registry mergerRegistry = RegistryImpl.createStandaloneInstance();
                    mergerRegistry.getHighestRankingMerger(diff).copyRightToLeft(diff, new BasicMonitor());
                }
                if (!this.modifiedFiles.contains(fileName)) {
                    this.modifiedFiles.add(fileName);
                }
                CommonUtils.saveResource(this.latestResource);
            }
        }
        this.detectedDifferences.clear();
        this.selectedDifferences.clear();
    }

    /**
     * Create ListSelectionDialog that contains all detected Differences. All unmatched Differences
     * are preselected.
     * 
     * @param selectionName
     *            name of ListSelectionDialog
     * @return ListSelectionDialog
     */
    public ListSelectionDialog createDiffSelectionDialog(String selectionName) {
        ListSelectionDialog dialog = new ListSelectionDialog(this.window.getShell(), this.detectedDifferences.toArray(),
                new ArrayContentProvider(), new DiffLabelProvider(), "Select Differences to merge:");
        dialog.setTitle("Refinement Selection: " + selectionName);
        dialog.setInitialSelections(this.unmatchedDifferences.toArray());
        return dialog;
    }

}
