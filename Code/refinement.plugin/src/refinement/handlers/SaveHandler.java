package refinement.handlers;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.xpath.XPathExpressionException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFileState;
import org.eclipse.core.resources.IFolder;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import provider.RefinementLabelProvider;
import provider.TreeContentProvider;
import refinement.model.refinements.AbstractRefinement;
import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.ComposedRefinement;
import refinement.model.refinements.Refinements;
import refinement.model.refinements.RefinementsFactory;
import refinement.model.refinements.RefinementsPackage;
import refinement.model.refinements.States;
import utils.DifferenceUtils;
import utils.LoggingUtils;
import utils.RefinementConstants;
import utils.CommonUtils;

/**
 * Handler that saves architectural changes to a refinement file. These changes prevent the PCM
 * files from being overwritten by changes of automatically extracted PCM files.
 * 
 * @author frborn
 * 
 */
public class SaveHandler extends AbstractHandler {
    private HashMap<Diff, AbstractRefinement> refinementsHash = new HashMap<Diff, AbstractRefinement>();
    private EList<AbstractRefinement> inputRefinements = new BasicEList<AbstractRefinement>();
    private Map<String, IFolder> folders = CommonUtils.getFolders();
    private IWorkbenchWindow window = CommonUtils.getWindow();
    private EList<String> modifiedFiles = new BasicEList<String>();

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        // Save names of modified PCM files to this list
        LoggingUtils.logging("Starting saving refinement steps");
        for (IFile pcmFile : CommonUtils.getAllIFiles(this.folders.get(RefinementConstants.PCM.getName()))) {
            // Get change history of current PCM IFile
            IFileState[] states = CommonUtils.getFileStates(pcmFile);
            // At least one session of refinement steps were applied
            if (states.length > 0) {
                // Load latest state of current PCM IFile as Resource
                ResourceSet latestResourceSet = new ResourceSetImpl();
                @SuppressWarnings("unused")
                Resource latestResource = latestResourceSet
                        .getResource(URI.createFileURI(pcmFile.getFullPath().toString()), true);
                // Load previous state of PCM IFile
                ResourceSet previousResourceSet = new ResourceSetImpl();
                // Create temporary Resource for previous version
                Resource previousResource = previousResourceSet.createResource(URI.createFileURI("tmp"));
                CommonUtils.loadFromFileState(states, previousResource, 0);
                IComparisonScope scope = new DefaultComparisonScope(previousResourceSet, latestResourceSet,
                        previousResourceSet);
                // Compare EMF models
                Comparison diffModel = EMFCompare.builder().build().compare(scope);
                // Check if refinement file exists for current PCM file
                String refinementFileName = pcmFile.getName() + "." + RefinementsPackage.eNAME;
                boolean refinementFileExists = CommonUtils
                        .fileExists(this.folders.get(RefinementConstants.REFINEMENT.getName()), refinementFileName);
                /*
                 * First Case: First session of refinement steps is saved, therefore states[] is of
                 * length one and contains only initial state of current PCM IFile at index 0 and no
                 * refinement file exists
                 */
                if (!refinementFileExists) {
                    saveRefinements(pcmFile, diffModel, refinementFileName);
                } else {
                    /*
                     * Second Case: Refinement steps for current file already exists. Update
                     * refinement value if difference of current diffModel affects a saved
                     * refinement. Add new refinement if it does not exists.
                     */
                    saveToExistingRefinements(pcmFile, diffModel, refinementFileName);
                }

            } else {
                // Case 3: No refinement Steps are applied for current PCM file
                continue;
            }
        }
        LoggingUtils.saveSuccess(this.modifiedFiles);
        this.modifiedFiles.clear();
        return null;
    }

    /**
     * If no manual refinement steps are saved a new refinements instance must be created and all
     * detected refinements must be added as Refinement. Those Refinement will be opened in an
     * eclipse ListSelectionDialog for a final selection. After confirmation all Refinements are
     * saved.
     * 
     * @param pcmFile
     *            IFile current PCM file
     * @param diffModel
     *            EMF Compare of two PCM models
     * @param refinementFileName
     *            Name of refinement file
     * 
     */
    private void saveRefinements(IFile pcmFile, Comparison diffModel, String refinementFileName) {
        // Create refinements instance and add all differences
        // as refinement instances to refinements instance
        Refinements refinements = RefinementsFactory.eINSTANCE.createRefinements();
        refinements.setId(pcmFile.getName());
        for (Diff diff : DifferenceUtils.reverseDiffs(diffModel)) {
            String oldEntityName = DifferenceUtils.extractStructuralFeature(diff, "entityName");
            AtomicRefinement refinement = RefinementsFactory.eINSTANCE.createAtomicRefinement();
            String referenceID = DifferenceUtils.extractStructuralFeature(diff, "id");
            // If referenceID can be created, create new refinement
            // and add it to refinements
            String refinementKind = diff.getKind().toString();
            String refinementKindDetail = DifferenceUtils.extractKindDetail(diff);
            String affectedElementType = DifferenceUtils.extractElementType(diff);
            if (referenceID != null) {
                refinement.setRefinement(DifferenceUtils.extractValue(diff), referenceID, diff.toString(),
                        refinementKind, oldEntityName, refinementKindDetail, affectedElementType);
                addRefinement(diff, refinement, refinements);
            }
        }
        openSaveDialog(pcmFile, refinementFileName, refinements);
    }

    /**
     * 
     * @param diff
     *            Current Difference
     * @param refinement
     *            Current refinement
     * @param oldRefinements
     *            Old refinements
     * @throws XPathExpressionException
     */
    private void addRefinement(Diff diff, AtomicRefinement refinement, Refinements oldRefinements) {
        /*
         * If refinement kind is of type "DELETE", check if an other refinement exists for
         * referenceID of given refinement. Replace first occurrence of this found refinement with
         * given refinement if this is true and remove all other occurrences.
         * 
         */
        if (refinement.getKind().contains("DELETE")) {
            EList<Integer> indexList = oldRefinements.findRefinementsByReference(refinement.getReferenceID());
            /*
             * If given refinement is not part of refinements the indexList is empty then add
             * refinement if it is not already part of refinements
             */
            if (indexList.isEmpty()) {
                if (oldRefinements.indexOfRefinement(refinement) == -1) {
                    if (!diff.getRequires().isEmpty()) {
                        ComposedRefinement composedRefinement = RefinementsFactory.eINSTANCE.createComposedRefinement();
                        composedRefinement.setName("Composition: " + refinement.getKind());
                        composedRefinement.setDescription(
                                "This composed refinement was created automatically based on the"
                                + " impact of deletion of element with ID='" + refinement.getReferenceID() + "'.");
                        composedRefinement.setDate(new Date());
                        composedRefinement.getAtomicRefinements().add(refinement);
                        this.refinementsHash.put(diff, composedRefinement);
                    }
                }
            } else {
                // If only one refinement with referenceID of given refinement
                // exists replace that refinement with given refinement
                if (indexList.size() == 1) {
                    AtomicRefinement oldRefinement = oldRefinements.getRefinement(indexList.get(0));
                    if (!oldRefinement.getKind().toUpperCase().contains("DELETE")) {
                        refinement.getValueStates().removeAll(refinement.getValueStates());
                        refinement.getValueStates().addAll(oldRefinement.getValueStates());
                        oldRefinements.removeRefinement(indexList.get(0));
                    }
                    if (!diff.getRequires().isEmpty()) {
                        ComposedRefinement composedRefinement = RefinementsFactory.eINSTANCE.createComposedRefinement();
                        composedRefinement.setName("Composition: " + refinement.getKind());
                        composedRefinement.setDescription(
                                "This composed refinement was created automatically based on the "
                                + "impact of deletion of element with ID='"
                                        + refinement.getReferenceID() + "'.");
                        composedRefinement.setDate(new Date());
                        composedRefinement.getAtomicRefinements().add(refinement);
                        this.refinementsHash.put(diff, composedRefinement);
                    }

                } else {
                    // Save index for replacing refinement
                    // and get oldRefinement by that ID
                    int replaceIndex = oldRefinements.getReplaceIndex(indexList);
                    AtomicRefinement oldRefinement = oldRefinements.getRefinement(replaceIndex);
                    refinement.addValueStates(oldRefinement.getValueStates());
                    // Reverse indexList to avoid index change
                    // due to removal of refinements
                    Collections.reverse(indexList);
                    // Remove refinements in reverse order
                    for (int i = 0; i < indexList.size(); i++) {
                        oldRefinements.removeRefinement(indexList.get(i));
                    }
                    // Replace first occurrence of refinement
                    // with given referenceID
                    if (!diff.getRequires().isEmpty()) {
                        ComposedRefinement composedRefinement = RefinementsFactory.eINSTANCE.createComposedRefinement();
                        composedRefinement.setName("Composition: " + refinement.getKind());
                        composedRefinement.setDescription(
                                "This composed refinement was created automatically based on the "
                                + "impact of deletion of element with ID='"
                                        + refinement.getReferenceID() + "'.");
                        composedRefinement.setDate(new Date());
                        composedRefinement.getAtomicRefinements().add(refinement);
                        this.refinementsHash.put(diff, composedRefinement);
                    }
                }
            }
        } else {
            // If given refinement is not of type "DELETE" add refinement
            // if it is not already part of refinements
            EList<Integer> indexList = oldRefinements.findRefinementsByReference(refinement.getReferenceID());
            EList<Diff> requiredBy = diff.getRequiredBy();
            if (requiredBy.isEmpty()) {
                if (indexList.isEmpty()) {
                    this.refinementsHash.put(diff, refinement);
                } else {
                    int replaceIndex = oldRefinements.getReplaceIndex(indexList);
                    AtomicRefinement oldRefinement = oldRefinements.getRefinement(replaceIndex);
                    refinement.addValueStates(oldRefinement.getValueStates());
                    this.refinementsHash.put(diff, refinement);
                    for (int i = 0; i < indexList.size(); i++) {
                        oldRefinements.removeRefinement(indexList.get(i));
                    }
                }
            } else {
                String kind = requiredBy.get(0).getKind().toString();
                if (kind.equals("DELETE")) {
                    refinement.setState(States.TODO);
                    if (this.refinementsHash.containsKey(requiredBy.get(0))) {
                        ComposedRefinement composedRefinement = (ComposedRefinement) this.refinementsHash
                                .get(requiredBy.get(0));
                        if (!composedRefinement.getAtomicRefinements().contains(refinement)) {
                            composedRefinement.getAtomicRefinements().add(refinement);
                            this.refinementsHash.put(requiredBy.get(0), composedRefinement);
                        }
                    } else {
                        ComposedRefinement composedRefinement = RefinementsFactory.eINSTANCE.createComposedRefinement();
                        composedRefinement.setName("Composition: " + refinement.getKind());
                        composedRefinement.setDescription(
                                "This composed refinement was created automatically based on the impact "
                                + "of deletion of element with ID='" + refinement.getReferenceID() + "'.");
                        composedRefinement.setDate(new Date());
                        if (!composedRefinement.getAtomicRefinements().contains(refinement)) {
                            composedRefinement.getAtomicRefinements().add(refinement);
                            this.refinementsHash.put(diff, composedRefinement);
                        }

                    }

                }
            }

        }
    }

    /**
     * Update refinement value if difference of current diffModel affects an already saved
     * refinement. Add new refinement if no old refinement exists.
     * 
     * @param pcmFile
     *            IFile of current PCM model
     * @param diffModel
     *            EMF Comparison
     * @param refinementFileName
     *            Name of refinement file
     */
    private void saveToExistingRefinements(IFile pcmFile, Comparison diffModel, String refinementFileName) {
        IFile refinementFile = this.folders.get(RefinementConstants.REFINEMENT.getName()).getFile(refinementFileName);
        Refinements refinements = CommonUtils.loadRefinements(refinementFile);
        for (Diff diff : DifferenceUtils.reverseDiffs(diffModel)) {
            String referenceID = DifferenceUtils.extractStructuralFeature(diff, "id");
            if (referenceID != null) {
                /*
                 * Check if reference of difference already exists in refinements if it exists
                 * overwrite value of corresponding refinement. Otherwise add new refinement to
                 * refinements.
                 */
                String refinementKindDetail = DifferenceUtils.extractKindDetail(diff);
                String refinementKind = diff.getKind().toString();
                String affectedElementType = DifferenceUtils.extractElementType(diff);
                String oldEntityName = DifferenceUtils.extractStructuralFeature(diff, "entityName");
                AtomicRefinement refinement = RefinementsFactory.eINSTANCE.createAtomicRefinement();
                refinement.setRefinement(DifferenceUtils.extractValue(diff), referenceID, diff.toString(),
                        refinementKind, oldEntityName, refinementKindDetail, affectedElementType);
                int refinementIndex = refinements.indexOfRefinement(refinement);
                if (refinementIndex > -1) {
                    LoggingUtils.logging("Warning: Refinement [" + refinement.getKind().toString() + "] for element"
                            + DifferenceUtils.extractStructuralFeature(diff, "id")
                            + " already exists. Value will be overwritten!");
                    AtomicRefinement oldRefinement = refinements.getRefinement(refinementIndex);
                    refinement.addValueStates(oldRefinement.getValueStates());
                    refinements.getAtomicRefinements().remove(refinementIndex);
                    addRefinement(diff, refinement, refinements);
                } else {
                    addRefinement(diff, refinement, refinements);
                }
            }
        }
        openSaveDialog(pcmFile, refinementFileName, refinements);
        LoggingUtils
                .logging("Succesfully added refinement steps to \"" + refinementFile.getFullPath().toString() + '"');
    }

    /**
     * Load all refinements as selection options in ListSelectionDialog. Add selected Refinement to
     * refinements and add current PCM file to modifiedFiles.
     * 
     * @param pcmFile
     *            IFile of current PCM file
     * @param refinementFileName
     *            Name of refinement file
     * @param refinements
     *            existing refinements object
     */
    private void openSaveDialog(IFile pcmFile, String refinementFileName, Refinements refinements) {
        String selectionName = pcmFile.getName();
        CheckedTreeSelectionDialog checkedtreeDialog = createCheckedTreeSelectionDialog(selectionName);
        if (checkedtreeDialog.open() == CheckedTreeSelectionDialog.OK) {
            Object[] result = checkedtreeDialog.getResult();
            for (Object selected : result) {
                refinements.addRefinement((AbstractRefinement) selected);
            }
            if (result.length > 0) {
                refinements.saveRefinements(this.folders.get(RefinementConstants.REFINEMENT.getName())
                        .getFile(refinementFileName).getLocation().toString());
                if (!this.modifiedFiles.contains(pcmFile.getName())) {
                    this.modifiedFiles.add(pcmFile.getName());
                }
            }
        }
        this.refinementsHash.clear();
        this.inputRefinements.clear();
    }

    /**
     * @param selectionName
     *            PCM model name
     * @return CheckedTreeSelectionDialog with detected refinements
     */
    public CheckedTreeSelectionDialog createCheckedTreeSelectionDialog(String selectionName) {
        CheckedTreeSelectionDialog checkedtreeDialog = new CheckedTreeSelectionDialog(window.getShell(),
                new RefinementLabelProvider(), new TreeContentProvider());
        Iterator<Entry<Diff, AbstractRefinement>> it = this.refinementsHash.entrySet().iterator();
        EList<AbstractRefinement> initialSelection = new BasicEList<AbstractRefinement>();
        HashMap<String, ComposedRefinement> tmpInputRefinement = new HashMap<String, ComposedRefinement>();
        /*
         * Fill HashMap with ComposedRefinement.getName() as key and all AtomicRefinements of every
         * ComposedRefinement with same name attribute and add all AtomicRefinement that are not
         * contained in a ComposedRefinement
         * 
         */
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getValue() instanceof ComposedRefinement) {
                ComposedRefinement composedRefinement = (ComposedRefinement) pair.getValue();
                if (!tmpInputRefinement.containsKey(composedRefinement.getName())) {
                    ComposedRefinement containerComposedRefinement = RefinementsFactory.eINSTANCE
                            .createComposedRefinement();
                    containerComposedRefinement.setName(composedRefinement.getName());
                    containerComposedRefinement.setDescription(composedRefinement.getDescription());
                    containerComposedRefinement.setDate(composedRefinement.getDate());
                    containerComposedRefinement.getAtomicRefinements()
                            .addAll(composedRefinement.getAtomicRefinements());
                    tmpInputRefinement.put(composedRefinement.getName(), containerComposedRefinement);
                } else {
                    ComposedRefinement containerComposedRefinement = tmpInputRefinement
                            .get(composedRefinement.getName());
                    containerComposedRefinement.getAtomicRefinements()
                            .addAll(composedRefinement.getAtomicRefinements());
                    tmpInputRefinement.put(composedRefinement.getName(), containerComposedRefinement);
                }

            } else if (pair.getValue() instanceof AtomicRefinement) {
                AtomicRefinement atomicRefinement = (AtomicRefinement) pair.getValue();
                initialSelection.add(atomicRefinement);
                this.inputRefinements.add((AbstractRefinement) pair.getValue());
            }
            it.remove(); // avoids a ConcurrentModificationException
        }
        /*
         * Fill HashMap with ComposedRefinement.getName() as key and all AtomicRefinements of every
         * ComposedRefinement with same name attribute
         * 
         */
        Iterator<Entry<String, ComposedRefinement>> tmpIt = tmpInputRefinement.entrySet().iterator();
        while (tmpIt.hasNext()) {
            Map.Entry pair = (Map.Entry) tmpIt.next();
            ComposedRefinement composedRefienement = (ComposedRefinement) pair.getValue();
            initialSelection.add(composedRefienement);
            initialSelection.addAll(composedRefienement.getAtomicRefinements());
            this.inputRefinements.add(composedRefienement);
            tmpIt.remove();
        }
        checkedtreeDialog.setInput(this.inputRefinements);
        checkedtreeDialog.setInitialSelections(initialSelection.toArray());
        checkedtreeDialog.setTitle("Refinement Selection: " + selectionName);
        checkedtreeDialog.setMessage("Select Refinements:");
        return checkedtreeDialog;
    }
}
