package utils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.internal.resources.Folder;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFileState;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import org.palladiosimulator.pcm.repository.Repository;
import org.palladiosimulator.pcm.repository.RepositoryComponent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.Refinements;

/**
 * Utility class of refinement plugin.
 * 
 * @author frborn
 *
 */
@SuppressWarnings("restriction")
public final class CommonUtils {
    /**
     * Private constructor to prevent instantiation
     */
    private CommonUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Return current Eclipse window
     * 
     * @return current window
     */
    public static IWorkbenchWindow getWindow() {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        return window;
    }

    /**
     * Create HashMap for "pcm", "src", "refinement" and "extracted" folder as iFolder.
     * 
     * 
     * @return HashMap with keys: "pcm", "src", "refinement", "extracted"
     */
    public static Map<String, IFolder> getFolders() {
        final Map<String, IFolder> folders = new HashMap<String, IFolder>();
        final IProject iProject = CommonUtils.getIProject();
        folders.put(RefinementConstants.PCM.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.PCM.getName()));
        folders.put(RefinementConstants.COMPONENT_ANALYSIS.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.COMPONENT_ANALYSIS.getName()));
        folders.put(RefinementConstants.REFINEMENT.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.REFINEMENT.getName()));
        folders.put(RefinementConstants.EXTRACTED.getName(),
                CommonUtils.getPCMIFolder(iProject, RefinementConstants.EXTRACTED.getName()));
        return folders;
    }

    /**
     * Save given Resource.
     * 
     * @param resource
     *            Resource to save
     */
    public static void saveResource(Resource resource) {
        try {
            resource.save(null);
            String[] path = resource.getURI().toString().split("/");
            LoggingUtils.logging("Saved \"" + path[path.length - 2] + "/" + path[path.length - 1] + "\"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return the file state history of given IFile.
     * 
     * @return IFileState[]
     */
    public static IPath getSelectedProject() {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        if (window != null) {
            IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
            Object firstElement = selection.getFirstElement();
            if (firstElement instanceof IAdaptable) {
                IProject project = (IProject) ((IAdaptable) firstElement).getAdapter(IProject.class);
                IPath path = project.getFullPath();
                return path;
            }
        }
        try {
            LoggingUtils.showNotification("Error: Please select a valid project!");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Return the file state history of given IFile.
     * 
     * @param iFile
     *            file to get states from
     * @return IFileState[]
     */
    public static IFileState[] getFileStates(final IFile iFile) {
        IFileState[] states = null;
        try {
            states = iFile.getHistory(new NullProgressMonitor());
        } catch (CoreException e) {
            e.printStackTrace();
        }
        return states;
    }

    /**
     * Load latest content of states (match: states[0]) to given resource.
     * 
     * @param states
     *            file states
     * @param resource
     *            Resource to load from
     * @param stateIndex
     *            index of state to get
     */
    public static void loadFromFileState(IFileState[] states, Resource resource, int stateIndex) {
        IFileState requestedFileState = states[stateIndex];
        try {
            resource.load(requestedFileState.getContents(), Collections.emptyMap());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get PCM IFolder that is located under project/refinement_pcms/{folderName}
     * 
     * @param iProject
     *            current project
     * @param folderName
     *            name of folder
     * @return IFolder
     */
    public static IFolder getPCMIFolder(final IProject iProject, String folderName) {
        final IFolder pcmFolder = iProject.getFolder("/refinement_pcms/" + folderName + "/");
        return pcmFolder;
    }

    /**
     * Return selected project when executing an event.
     * 
     * @return selected project as IProject
     */
    public static IProject getIProject() {
        final String projectName = getSelectedProject().toString();
        final IWorkspaceRoot currentWorkspace = ResourcesPlugin.getWorkspace().getRoot();
        final IProject selectedProject = currentWorkspace.getProject(projectName);
        return selectedProject;
    }

    /**
     * Return all IFiles that are directly located in given IFolder.
     * 
     * @param iFolder
     *            folder to get all files from
     * @return all IFiles that are located in given IFolder
     */
    public static EList<IFile> getAllIFiles(final IFolder iFolder) {
        EList<IFile> iFiles = new BasicEList<IFile>();
        try {
            IResource[] iResource = iFolder.members();
            for (IResource resource : iResource) {
                if (resource instanceof IFile) {
                    iFiles.add((IFile) resource);
                } else if (resource instanceof Folder) {
                    IFile iFile = ((Folder) resource).getFile(resource.getFullPath().toString());
                    iFiles.add(iFile);
                }
            }
        } catch (CoreException e1) {
            e1.printStackTrace();
        }
        return iFiles;
    }

    /**
     * Return all component names that are extracted of "component.xml" of source sub folders
     * following the assumption, that all components of the performance model must contain such a
     * BasicComponent.
     * 
     * @param iFolder
     *            Folder
     * @return all component names
     */
    public static EList<String> getComponentNames(final IFolder iFolder) {
        EList<String> componentNames = new BasicEList<String>();
        File rootFolder = new File(iFolder.getLocationURI());
        try {
            Collection<File> files = FileUtils.listFiles(rootFolder, null, true);
            for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
                File file = (File) iterator.next();
                if (file.getName().equals("component.xml")) {
                    Document components = XMLUtils.getXMLTreeFromFile(file);
                    Node componentNode = XMLUtils.getElementsByTagName(components, "component").get(0);
                    // Last segment of component@id is component name
                    // (component@id is separated by ".")
                    String[] componentIDList = (((Element) componentNode).getAttribute("id").toString()).split("\\.");
                    String componentName = componentIDList[componentIDList.length - 1];
                    componentNames.add(componentName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return componentNames;
    }

    /**
     * Split fileName of format *.{PCM} at "." and return {PCM}.
     * 
     * @param pcmFile
     *            Current PCM file
     * @return file name
     */
    public static String getFileName(IFile pcmFile) {
        return pcmFile.getName().split("\\.")[1];
    }

    /**
     * Find file in given folder with given extension. Uses lookupIndex to split the file name and
     * look at index for given file extension.
     * 
     * @param folder
     *            Folder too search in
     * @param string
     *            Extension of file to search for
     * @param lookupIndex
     *            Split index of where the extension is located
     * @return IFile that matches extension
     */
    public static IFile getCorrespondingFile(final IFolder folder, String string, int lookupIndex) {
        for (IFile file : getAllIFiles(folder)) {
            String fileExtension = file.getName().toString().split("\\.")[lookupIndex];
            if (fileExtension.equals(string)) {
                return file;
            }
        }
        return null;
    }

    /**
     * Create ResourceSet with content of given filePath, parse it to
     * org.palladiosimulator.pcm.repository.Repository and return that Repository.
     * 
     * @param filePath
     *            path name
     * @return org.palladiosimulator.pcm.repository.Repository
     */
    public static Repository loadRepository(final String filePath) {
        // Register the XMI resource factory for the .refinements extension
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put(RefinementConstants.REFINEMENT.getName(), new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.getResource(URI.createURI(filePath), true);
        Repository repository = (Repository) resource.getContents().get(0);
        return repository;
    }

    /**
     * Index of repository_BasicComponent with an entityName that matches componentName. Returns -1
     * if no component exists.
     * 
     * @param components
     *            Components of Repository
     * @param componentName
     *            component name
     * @return index of BasicComponent
     */
    public static Integer repositoryIndexOfComponent(EList<RepositoryComponent> components, String componentName) {
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).getEntityName().toLowerCase().equals(componentName.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Index of component with an entityName that matches componentName. Returns -1 if no component
     * exists.
     * 
     * @param components
     *            Components of Repository
     * @param componentName
     *            component name
     * @return index of component
     */
    public static Integer repositoryIndexOfCreatedComponent(EList<String> components, String componentName) {
        for (int i = 0; i < components.size(); i++) {
            if (components.get(i).toLowerCase().equals(componentName.toLowerCase())) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Save repository as IFile.
     * 
     * @param repositoryFile
     *            repository IFile
     * @param repository
     *            modified Repository object
     */
    public static void saveRepository(IFile repositoryFile, Repository repository) {
        ResourceSet resourceSet = new ResourceSetImpl();
        Resource resource = resourceSet.createResource(URI.createFileURI(repositoryFile.getFullPath().toString()));
        resource.getContents().add((EObject) repository);
        try {
            resource.save(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Extract component name from attribute "name" of "service" element.
     * 
     * @param service
     *            service node
     * @return String value of "name" attribute of service
     */
    public static String extractComponentName(Node service) {
        String serviceName = ((Element) service).getAttribute("name");
        String[] serviceNameList = serviceName.split("\\.");
        String componentName = serviceNameList[serviceNameList.length - 1];
        return componentName;
    }

    /**
     * Return index of refinement that contains componentNames.toLowerCase in valueStates. Returns
     * -1 if no component exists.
     * 
     * @param refinementFile
     *            IFile refinement file
     * @param componentName
     *            component name string
     * @return index of Refinement
     */
    public static Integer refinementIndexOfComponent(IFile refinementFile, String componentName) {
        Refinements oldRefinements = loadRefinements(refinementFile);
        EList<AtomicRefinement> refinements = oldRefinements.getAtomicRefinements();
        for (int i = 0; i < refinements.size(); i++) {
            EList<String> valueStates = refinements.get(i).getValueStates();
            for (int j = 0; j < valueStates.size(); j++) {
                if (valueStates.get(j).toLowerCase().contains(componentName.toLowerCase())) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Load refinement from IFile.
     * 
     * @param refinementFile
     *            IFile refinement file
     * @return Refinements Loaded Refinements
     */
    public static Refinements loadRefinements(IFile refinementFile) {
        String filePath = refinementFile.getFullPath().toString();
        // Register the XMI resource factory for the .refinements extension
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put(RefinementConstants.REFINEMENTS.getName(), new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.getResource(URI.createURI(filePath), true);
        // Create refinements and add all refinements from resource
        Refinements refinements = (Refinements) resource.getContents().get(0);
        return refinements;
    }

    /**
     * Check if fileName exists as file in given IFolder.
     * 
     * @param ifolder
     *            Folder to search in
     * @param fileName
     *            file name
     * @return Boolean
     */
    public static boolean fileExists(final IFolder ifolder, String fileName) {
        return ifolder.getFile(fileName).exists();
    }

    /**
     * @param dialogTitle
     *            title of selectionDialog
     * @param dialogInfo
     *            information of selectionDialog
     * @param potentialComponents
     *            selection options
     * @return ListSelectionDialog selectionDialog object
     */
    public static ListSelectionDialog createStringSelectionDialog(String dialogTitle, String dialogInfo,
            EList<String> potentialComponents) {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        ListSelectionDialog dialog = new ListSelectionDialog(window.getShell(), potentialComponents.toArray(),
                new ArrayContentProvider(), new LabelProvider(), dialogInfo);
        dialog.setTitle("Refinement Selection: " + dialogTitle);
        dialog.setInitialSelections(potentialComponents.toArray());
        return dialog;
    }

    /**
     * Open ListSelectionDialog that contains all options as strings. All selected options are added
     * to selectedOptions after confirmation.
     * 
     * @param potentialComponents
     *            selection options
     * @param dialogInfo
     *            information for selectionDialog
     * @param selectionDialog
     *            selection dialog name
     * @return List of all selected options of dialog.
     * @throws ExecutionException
     *             for getActiveWorkbenchWindowChecked
     */
    public static EList<String> openStringSelectionDialog(EList<String> potentialComponents, String selectionDialog,
            String dialogInfo) throws ExecutionException {
        // List for saving component names that are chosen to be created
        EList<String> selectedOptions = new BasicEList<String>();
        ListSelectionDialog dialog = createStringSelectionDialog(selectionDialog, dialogInfo, potentialComponents);
        // Add all selected names to selectedComponents
        if (dialog.open() == ListSelectionDialog.OK) {
            Object[] result = dialog.getResult();
            for (Object selected : result) {
                selectedOptions.add((String) selected);
            }
        }
        return selectedOptions;
    }

    public static void saveDiffModel(Comparison diffModel, IFile file) {
        Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
        Map<String, Object> m = reg.getExtensionToFactoryMap();
        m.put("comparison", new XMIResourceFactoryImpl());
        ResourceSet resSet = new ResourceSetImpl();
        Resource resource = resSet.createResource(URI.createFileURI(file.getFullPath().toString()));
        resource.getContents().add(diffModel);
        try {
            resource.save(Collections.EMPTY_MAP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
