package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.internal.spec.AttributeChangeSpec;
import org.eclipse.emf.compare.internal.spec.ReferenceChangeSpec;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.ResourceSet;

import refinement.model.refinements.AtomicRefinement;
import refinement.model.refinements.Refinements;

/**
 * Utility class that is responsible for operations on differences.
 * 
 * @author frborn
 *
 */
@SuppressWarnings("restriction")
public final class DifferenceUtils {
    /**
     * Private constructor to prevent instantiation
     */
    private DifferenceUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Extract EStructuralFeature feature of given difference as String. First try to get
     * EStructuralFeature "value" from given difference and get EStructuralFeature feature as
     * String. If EStructuralFeature "value" does not exist or can not be cast, get
     * EStructuralFeature feature of diff.getMatch().getLeft() as String.
     * 
     * @param diff
     *            Difference
     * @param feature
     *            String of feature name
     * @return EStructuralFeature feature as String
     */
    public static String extractStructuralFeature(Diff diff, String feature) {
        String extractedFeature = null;
        // Get feature from value of difference
        // if difference is of type "ReferenceChangeSpec",
        // which is the case if kind is "DELETE" or "ADD"
        try {
            EStructuralFeature featureEValue = null;
            featureEValue = diff.eClass().getEStructuralFeature("value");
            if (featureEValue != null) {
                EObject eValue = (EObject) diff.eGet(featureEValue);
                EStructuralFeature featureElement = eValue.eClass().getEStructuralFeature(feature);
                if (featureElement != null) {
                    extractedFeature = (String) eValue.eGet(featureElement);
                }
            }
        } catch (ClassCastException e) {
            // Get feature from left element if no object
            // could be cast from "value"
            Match match = diff.getMatch();
            EObject left = match.getLeft();
            if (left != null) {
                EStructuralFeature featureElement = left.eClass().getEStructuralFeature(feature);
                if (featureElement != null) {
                    extractedFeature = (String) left.eGet(featureElement);
                }
            }
        }
        return extractedFeature;
    }

    /**
     * Extract value of EObject.
     * 
     * @param diff
     *            Current Difference
     * @return value attribute as string
     */
    public static String extractValue(Diff diff) {
        Object value = null;
        Object res = null;
        value = diff.eGet(diff.eClass().getEStructuralFeature("value"));
        try {
            MinimalEObjectImpl valueObject = (MinimalEObjectImpl) value;
            res = valueObject.dynamicGet(1);
        } catch (ClassCastException e) {

        }
        if (res != null) {
            return res.toString();
        }
        return value.toString();
    }

    /**
     * Returns index of refinement with a reference that valueState contains the entityName of the
     * difference. Return -1 if no refinement exists.
     * 
     * @param refinements
     *            Refinements
     * @param diff
     *            Difference
     * 
     * @return Integer index
     */
    public static int refinementIndexOfDifference(Refinements refinements, Diff diff) {
        for (int i = 0; i < refinements.getAtomicRefinements().size(); i++) {
            AtomicRefinement refinement = refinements.getAtomicRefinements().get(i);
            // Return index of refinement that has a valueState that matches
            // entityName of element that is affected by this difference
            String referenceType = extractElementType(diff);
            if (refinement.getReferenceType().contains(referenceType)) {
                String diffValue = extractValue(diff);
                EList<String> valueStates = refinement.getValueStates();
                for (int j = 0; j < valueStates.size(); j++) {
                    if (valueStates.get(j).toLowerCase().contains(diffValue.toLowerCase())) {
                        return i;
                    }
                }
            }

        }
        return -1;
    }

    /**
     * Extract detailed information about the Difference kind
     * 
     * @param diff
     *            EMF Compare Difference
     * @return detailed difference kind
     */
    public static String extractKindDetail(Diff diff) {
        String kindDetail = "";
        if (diff instanceof AttributeChangeSpec) {
            AttributeChangeSpec attributeChangeSpec = (AttributeChangeSpec) diff;
            kindDetail = attributeChangeSpec.getAttribute().getName();
        } else if (diff instanceof ReferenceChangeSpec) {
            ReferenceChangeSpec referenceChangeSpec = (ReferenceChangeSpec) diff;
            kindDetail = referenceChangeSpec.getReference().getName();
        }
        return kindDetail;
    }

    /**
     * Extract detailed information about the Difference kind
     * 
     * @param diff
     *            EMF Compare Difference
     * @return detailed difference kind
     */
    public static String extractElementType(Diff diff) {
        String elementType = "";
        Match match = diff.getMatch();
        Object right = match.getRight();

        if (right != null) {
            Object rightClass = right.getClass();
            elementType = rightClass.toString();
        } else {
            Object left = match.getLeft();

            if (left != null) {
                Object leftClass = left.getClass();
                elementType = leftClass.toString();
            }
        }
        return elementType;
    }

    /**
     * 
     * Reverse Differenced of diffModel in order to create high level refinements first
     * 
     * @param diffModel
     *            EMF Comparison
     * @return reverse difference EList
     */
    public static List<Diff> reverseDiffs(Comparison diffModel) {
        List<Diff> reverseDiffs = new ArrayList<Diff>();
        reverseDiffs.addAll(diffModel.getDifferences());
        Collections.reverse(reverseDiffs);
        return reverseDiffs;
    }

    /**
     * Execute EMF compare between latest resource set and extracted resource set.
     * 
     * @param latestResourceSet
     *            ResourceSet of current PCM model
     * @param extractedResourceSet
     *            ResourceSet of extracted PCM model
     * @return Comparison diffModel
     */
    public static Comparison compareResources(ResourceSet latestResourceSet, ResourceSet extractedResourceSet) {
        IComparisonScope scope = new DefaultComparisonScope(latestResourceSet, extractedResourceSet, latestResourceSet);
        IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);
        IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
        IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
        matchEngineFactory.setRanking(20);
        IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
        matchEngineRegistry.add(matchEngineFactory);
        Comparison diffModel = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build()
                .compare(scope);
        return diffModel;
    }
}
