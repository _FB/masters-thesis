package utils;

import java.sql.Timestamp;
import java.util.Date;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

/**
 * Utility class that is responsible for logging.
 * 
 * @author frborn
 *
 */
public final class LoggingUtils {
    /**
     * Private constructor to prevent instantiation
     */
    private LoggingUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Log message to development and plugin console.
     * 
     * @param message
     *            String
     */
    public static void logging(String message) {
        MessageConsole myConsole = findConsole("refinement console");
        MessageConsoleStream out = myConsole.newMessageStream();
        out.println("[" + getCurrentTimestamp() + "]: " + message);
        System.out.println("[" + getCurrentTimestamp() + "]: " + message);
    }

    /**
     * Get current time stamp.
     * 
     * @return timestamp
     */
    private static Timestamp getCurrentTimestamp() {
        Date date = new Date();
        long time = date.getTime();
        Timestamp timestamp = new Timestamp(time);
        return timestamp;
    }

    /**
     * Get console of Plugin.
     * 
     * @param name
     *            Console name
     * @return Plugin console
     */
    private static MessageConsole findConsole(String name) {
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();
        for (int i = 0; i < existing.length; i++) {
            if (name.equals(existing[i].getName())) {
                return (MessageConsole) existing[i];
            }
        }
        // no console found, so create a new one
        MessageConsole myConsole = new MessageConsole(name, null);
        conMan.addConsoles(new IConsole[] { myConsole });
        return myConsole;
    }

    /**
     * Open pop up window that displays given message.
     * 
     * @param message
     *            String
     * @throws ExecutionException
     *             showNotification
     */
    public static void showNotification(String message) throws ExecutionException {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        MessageDialog.openInformation(window.getShell(), "Refinement Plugin", message);
    }

    /**
     * Success message for SaveHandler.
     * 
     * @param modifiedFiles
     *            EList of modified file names
     * @throws ExecutionException
     *             showNotification
     */
    public static void saveSuccess(EList<String> modifiedFiles) throws ExecutionException {
        if (!modifiedFiles.isEmpty()) {
            logging("Updated: " + modifiedFiles.toString());
            showNotification("Updated: " + modifiedFiles.toString());
        } else {
            logging("Warning: No refinement steps detected. Updating of refinement steps completed");
            showNotification("Info: No refinement steps detected. Nothing was saved.");
        }
    }

    /**
     * Success message for RefinementHandler.
     * 
     * @param modifiedFiles
     *            EList of modified file names
     * @throws ExecutionException
     *             showNotification
     */
    public static void refinementSucess(EList<String> modifiedFiles) throws ExecutionException {
        if (!modifiedFiles.isEmpty()) {
            logging("Updated: " + modifiedFiles.toString());
            showNotification("Updated: " + modifiedFiles.toString());
        } else {
            logging("Warning: No differences between extracted and current PCM files detected");
            showNotification("Info: No differences between extracted and current PCM files detected.");
        }
    }

    /**
     * Success message for ComponentHandler.
     * 
     * @param createdComponents
     *            EList of names of new component names
     * @throws ExecutionException
     *             showNotification
     */
    public static void componentSucess(EList<String> createdComponents) throws ExecutionException {
        if (!createdComponents.isEmpty()) {
            logging("Created: " + createdComponents.toString());
            showNotification("Created BasicComponents and Interfaces for: " + createdComponents.toString());
        } else {
            logging("Info: No components were created.");
            showNotification("Info: No components were created.");
        }
    }
}
