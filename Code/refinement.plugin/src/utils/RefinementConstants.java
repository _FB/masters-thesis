package utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * 
 * @author frborn
 *
 */
public enum RefinementConstants implements Enumerator {

    REPOSITORY(0, "repository", "repository"),

    PCM(1, "pcm", "pcm"),

    REFINEMENT(2, "refinement", "refinement"),

    REFINEMENTS(3, "refinements", "refinements"),

    EXTRACTED(4, "extracted", "extracted"),

    COMPONENT_ANALYSIS(5, "src", "src"),

    SYSTEM(6, "system", "system");

    public static final int REPOSITORY_VALUE = 0;
    public static final int PCM_VALUE = 1;
    public static final int REFINEMENT_VALUE = 2;
    public static final int REFINEMENTS_VALUE = 3;
    public static final int EXTACTED_VALUE = 4;
    public static final int COMPONENT_ANALYSIS_VALUE = 5;
    public static final int SYSTEM_VALUE = 6;

    /**
     * An array of all the '<em><b>States</b></em>' enumerators. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    private static final RefinementConstants[] VALUES_ARRAY = new RefinementConstants[] { REPOSITORY, PCM, REFINEMENT,
            REFINEMENTS, EXTRACTED, COMPONENT_ANALYSIS, SYSTEM };

    /**
     * A public read-only list of all the '<em><b>States</b></em>' enumerators. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<RefinementConstants> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified literal value. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param literal
     *            the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static RefinementConstants get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            RefinementConstants result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified name. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name
     *            the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static RefinementConstants getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            RefinementConstants result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>States</b></em>' literal with the specified integer value. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value
     *            the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static RefinementConstants get(int value) {
        switch (value) {
        case REPOSITORY_VALUE:
            return REPOSITORY;
        case PCM_VALUE:
            return PCM;
        case REFINEMENT_VALUE:
            return REFINEMENT;
        case REFINEMENTS_VALUE:
            return REFINEMENTS;
        case EXTACTED_VALUE:
            return EXTRACTED;
        case COMPONENT_ANALYSIS_VALUE:
            return COMPONENT_ANALYSIS;
        case SYSTEM_VALUE:
            return SYSTEM;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private RefinementConstants(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }
}
