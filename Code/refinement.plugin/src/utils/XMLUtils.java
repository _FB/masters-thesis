package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Utility class for handling XML files and elements.
 */
public final class XMLUtils {
    /**
     * Private constructor to prevent instantiation
     */
    private XMLUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Get XML tree as document from given IFile.
     * 
     * @param file
     *            XML IFile
     * @return XML tree
     * @throws ParserConfigurationException
     */
    public static Document getXMLTree(final IFile file) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.parse((InputStream) file.getContents());
            document.getDocumentElement().normalize();
            return document;
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get XML tree as document from given IFile.
     * 
     * @param file
     *            XML file
     * @return XML tree
     * @throws ParserConfigurationException
     */
    public static Document getXMLTreeFromFile(final File file) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream targetStream = new FileInputStream(file);
            Document document = dBuilder.parse(targetStream);
            document.getDocumentElement().normalize();
            return document;
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Save XML tree at filePath.
     * 
     * @param document
     *            Document object
     * @param filePath
     *            file path
     */
    public static void saveXMLDocument(Document document, String filePath) {
        Transformer transformer;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(filePath));
            Source input = new DOMSource(document);
            transformer.transform(input, output);
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get List of all elements with tag equals tagName from given node.
     * 
     * @param node
     *            Starting node
     * @param tagName
     *            tag name as String
     * @return List of all nodes with given tagName
     */
    public static List<Node> getElementsByTagName(Node node, String tagName) {
        EList<Node> result = new BasicEList<Node>();
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            // Add childNode to result list if element tag equals searchString
            if (childNode.getNodeName().toString().equals(tagName)) {
                result.add(childNode);
            }
            // Call method recursively for every child node and add elements
            // that't tag matches searchString
            result.addAll(getElementsByTagName(childNode, tagName));
        }
        return result;
    }
}
