# Master's Thesis
This master's thesis with the title ''Continuous Refinement of Automatically Extracted Performance Models' was developed by Frederic Born in 2019 at the Karlsruher Institute of Technology (KIT) in cooperation with the Forschungszentrum Informatik (FZI).gi

## Abstract
Nowadays a steadily increasing amount of companies is facing the problem that one or more of their legacy systems is based on a monolithic software architecture that has become increasingly complex over the years. This increased complexity means that the evolution of the legacy system is associated with increased efforts and costs. These increasing costs can result in the product becoming more expensive and customers looking for cheaper alternatives. Many companies are therefore forced to move from the monolithic architecture of their legacy systems to a more appropriate architecture.

A successful architecture pattern, that has prevailed in recent years, is the microservices architecture, in which each functionality of a system is migrated to its own microservice. This enables a decoupled evolution of individual services of the software system and thus lower costs in comparison to the evolution of a single monolithic system. However, it has been shown that the migration process from a monolithic architecture to a microservices architecture is a complex and error-prone process.

The goal of this thesis is to support such a migration process by developing a concept for a continuous refinement of automatically extracted architectural performance models, that is implemented by a prototypical plugin. These extracted models should help to prevent performance regression and to check the conformity of the software architecture. This thesis includes a concept for applying and storing manual refinement steps on extracted performance models. In addition, this thesis enables the plugin user to merge automatically extracted performance models with a performance model that is continuously refined. An approach for the integration of the developed concept into a CI environment is also provided.

The approach of this work was evaluated in cooperation with Capgemini SE on the basis of a case study. This includes checking whether all requirements have been met and whether the creation of all possible manual refinement steps are correctly supported by the plugin.

## Contrbution
This thesis provides a concept for saving architectural design decisions as refinement steps to a performance model. An implementation of this concept was modelled with the help of the Eclipse Modeling Framework and includes refinement steps with different granularity. With the help of this refinement model, an approach was developed that makes it possible to merge an existing performance model with an automatically generated performance model without overwriting elements of the existing performance model that have previously been manually refined. By implementing this approach as an Eclipse plugin it is successfully integrated into the Palladio workbench and can be integrated into a continuous integration environment. A corresponding concept for the integration into such an environment is also presented in detail and can be implemented as future work.

## Structure
* **Artifacts:** Contains all important artifacts created in the context of this thesis, such as problem statement, thesis and presentation slides.
* **Code:** Contains all source code of the developed plugin. All code is published under the Eclipse Public License - v 2.0.
* **Plugins:** Contains all exportet `*.jar` files, that can be installed into every Eclipse instance by copying these files in to the `plugins` directory of the Eclipse instance.

## Contact
This master's thesis was developed by Frederic Born. If you have any further questions, please contact via [Mail](mailto:frederic.b.born@web.de).